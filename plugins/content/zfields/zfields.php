<?php
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' );

class plgContentZfields extends JPlugin {

        /**
         * Load the language file on instantiation.
         * Note this is only available in Joomla 3.1 and higher.
         * If you want to support 3.0 series you must override the constructor
         *
         * @var boolean
         * @since 3.1
         */

        protected $autoloadLanguage = true;

        function onContentPrepareForm($form, $data) {

                $app = JFactory::getApplication();
                $option = $app->input->get('option');

                switch($option) {

                    case 'com_content':
                        if ($app->isAdmin()) {

                                //Hard-coded array of cateogry IDs. You could easily incorporate
                                //a category field in the plugin XML to dynamically create this array
                                $selected_categories = array(9);

                                JForm::addFormPath(__DIR__ . '/forms');

                                //Show specific forms based on categories
                                // if( is_array($data) && in_array($data['catid'], 9) ){
                                //
                                //     $form->loadFile('tenants', false);
                                //
                                // }else{

                                    $form->loadFile('content', false);

                                // }  

                        }
                        return true;

                }

                return true;

        }

}
?>
