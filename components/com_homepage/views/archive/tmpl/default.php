<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');


if(isset($_GET['years'])) {
	$years = (int)$_GET["years"];
} else {
	$years = NULL;
}

if(isset($_GET['breed'])) {
	$breed = (int)$_GET["breed"];
} else {
	$breed = NULL;
}

if(isset($_GET['male'])) {
	$male = (int)$_GET["male"];
} else {
	$male = NULL;
}

if(isset($_GET['female'])) {
	$female = (int)$_GET["female"];
} else {
	$female = NULL;
}

if ( isset($_GET['years']) || isset($_GET['breed']) || isset($_GET['male']) || isset($_GET['female'])) {
	echo "<script>jQuery(window).load(function(){ filterCategoryItem(); });</script>";
}


$select_category = JRequest::getVar('mycategory');

$avaivable_text = JRequest::getVar('avaivable_text');
$interesting_text = JRequest::getVar('interesting_text');
$reserved_text = JRequest::getVar('reserved_text');
$sold_text = JRequest::getVar('sold_text');
$stay_text = JRequest::getVar('stay_text');

$status_title = JRequest::getVar('status_title');
$gender_title = JRequest::getVar('gender_title');
$color_title = JRequest::getVar('color_title');
$birth_title = JRequest::getVar('birth_title');

$male_title = JRequest::getVar('male_title');
$female_title = JRequest::getVar('female_title');

$read_more_text = JRequest::getVar('read_more_text');

$no_left_kittens = JRequest::getVar('no_left_kittens');

$more_kittens_menu = JRequest::getVar('more_kittens_menu');
$more_kittens_button = JRequest::getVar('more_kittens_button');

$our_plans_menu = JRequest::getVar('our_plans_menu');
$up_plans_button = JRequest::getVar('up_plans_button');

$old_kittens_menu = JRequest::getVar('old_kittens_menu');
$old_kitten_button = JRequest::getVar('old_kitten_button');

$color_field_id = JRequest::getVar('color_field_id');
$color_code_field_id = JRequest::getVar('color_code_field_id');
$status_field_id = JRequest::getVar('status_field_id');
$gender_field_id = JRequest::getVar('gender_field_id');
$country_field_id = JRequest::getVar('country_field_id');

if (!function_exists('GetCategoryWithFeaturedArticle')) {
	function GetCategoryWithFeaturedArticle($main_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__categories.*')
		->from($db->quoteName('#__categories'))
		->join('LEFT', $db->quoteName('#__content') . ' ON (' . $db->quoteName('#__categories.id') . ' = ' . $db->quoteName('#__content.catid') . ')')
		->where($db->quoteName('#__categories.parent_id') . ' = ' . $db->quote($main_id))
		->group($db->quoteName('#__categories.title'))
		->order($db->quoteName('#__categories.title') . ' DESC');
		$db->setQuery($query_cat);
		$all_cats = $db->loadObjectList();
		return $all_cats;
	}
}

if (!function_exists('GetCategoryArticles')) {
	function GetCategoryArticles($id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__content.*')
		->from($db->quoteName('#__content'))
		->where($db->quoteName('#__content.catid') . ' = ' . $db->quote($id))
		->order($db->quoteName('#__content.featured') . ' DESC');
		$db->setQuery($query_cat);
		$all_cats = $db->loadObjectList();
		return $all_cats;
	}
}

if (!function_exists('GetCountLeftArticle')) {
	function GetCountLeftArticle($main_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select( array('COUNT(*)') )
		->from($db->quoteName('#__content'))
		->where($db->quoteName('#__content.catid') . ' = ' . $db->quote($main_id), 'AND')
		->where($db->quoteName('#__content.featured') . ' = 1 ' );
		$db->setQuery($query_cat);
		$all_cats = $db->loadResult();
		return $all_cats;
	}
}

if (!function_exists('GetMainFilter')) {
	function GetMainFilter($id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__tags.title')
		->from($db->quoteName('#__tags'))
		->where($db->quoteName('#__tags.id') . ' = ' . $db->quote($id));
		$db->setQuery($query_cat);
		$all_cats = $db->loadResult();
		return $all_cats;
	}
}
if (!function_exists('GetFilterList')) {
	function GetFilterList($id,$order) {
		switch ($order) {
			case '0':
			$order_value = 'ASC';
			break;
			case '1':
			$order_value = 'DESC';
			break;
			default:
			$order_value = 'ASC';
			break;
		}
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select(array('#__tags.id','#__tags.title'))
		->from($db->quoteName('#__tags'))
		->where($db->quoteName('#__tags.parent_id') . ' = ' . $db->quote($id))
		->order($db->quoteName('#__tags.title') . ' ' . $order_value);
		$db->setQuery($query_cat);
		$all_cats = $db->loadObjectList();
		return $all_cats;
	}
}

if (!function_exists('getCustomFields')) {
	function getCustomFields($id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__fields_values.*')
		->from($db->quoteName('#__fields_values'))
		->where($db->quoteName('#__fields_values.item_id') . ' = ' . $db->quote($id));
		$db->setQuery($query_cat);
		$results = $db->loadObjectList();
		return $results;
	}
}
if (!function_exists('getCustomFieldsValue')) {
	function getCustomFieldsValue($field_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__fields.*')
		->from($db->quoteName('#__fields'))
		->where($db->quoteName('#__fields.id') . ' = ' . $db->quote($field_id));
		$db->setQuery($query_cat);
		$results = $db->loadObjectList();
		return $results;
	}
}

?>
<div id="avaivable-kittens-filter" class="container-fluid" data-spy="affix" data-offset-top="170">
	<div class="container">
		<div class="col-lg-12 filter-content">
			<div class="row">
				<div class="col-lg-3 filter-year">
					<select class="selectpicker year" title="<?php echo GetMainFilter(4); ?>" multiple data-max-options="1" data-size="5">
						<?php 
						foreach (GetFilterList(4,1) as $filter_item) {
							?>
							<option value="<?php echo $filter_item->id; ?>" <?php if ($years == $filter_item->id) { ?>selected <?php }; ?>><?php echo $filter_item->title; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-3 filter-letter">
					<select class="selectpicker letter" title="<?php echo GetMainFilter(5); ?>" multiple data-max-options="1" data-size="5">
						<?php 
						foreach (GetFilterList(5,0) as $filter_item) {
							?>
							<option value="<?php echo $filter_item->id; ?>" <?php if ($breed == $filter_item->id) { ?>selected <?php }; ?>><?php echo $filter_item->title; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-3 filter-father">
					<select class="selectpicker father" title="<?php echo GetMainFilter(2); ?>" multiple data-max-options="1" data-size="5">
						<?php 
						foreach (GetFilterList(2,0) as $filter_item) {
							?>
							<option value="<?php echo $filter_item->id; ?>" <?php if ($male == $filter_item->id) { ?>selected <?php }; ?>><?php echo $filter_item->title; ?></option>
							<?php
						}
						?>
					</select>
				</div>
				<div class="col-lg-3 filter-mother">
					<select class="selectpicker mother" title="<?php echo GetMainFilter(3); ?>" multiple data-max-options="1" data-size="5">
						<?php 
						foreach (GetFilterList(3,0) as $filter_item) {
							?>
							<option value="<?php echo $filter_item->id; ?>" <?php if ($female == $filter_item->id) { ?>selected <?php }; ?>><?php echo $filter_item->title; ?></option>
							<?php
						}
						?>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="avaivable-kittens" class="container">
	<div class="row">
		<div class="col-lg-12 content">
			<div class="row">
				<?php $all_items = GetCategoryWithFeaturedArticle($select_category); ?>
				<?php foreach ($all_items as $item): ?>

					<?php 

					$categoryTags = new JHelperTags;
					$categoryTags->getItemTags('com_content.category', $item->id);


					for ($i=0; $i < count($categoryTags->itemTags) ; $i++) { 

						if ($categoryTags->itemTags[$i]->parent_id == '4') {
							$year = $categoryTags->itemTags[$i]->id;
						}
						if ($categoryTags->itemTags[$i]->parent_id == '5') {
							$year_letter = $categoryTags->itemTags[$i]->id;
						}
						if ($categoryTags->itemTags[$i]->parent_id == '2') {
							$father = $categoryTags->itemTags[$i]->id;
						}
						if ($categoryTags->itemTags[$i]->parent_id == '3') {
							$mother = $categoryTags->itemTags[$i]->id;
						}
					}
					?>


					<div id="<?php echo $item->id; ?>" class="col-lg-12 category-item" data-year="<?php echo $year; ?>" data-letter="<?php echo $year_letter; ?>" data-father="<?php echo $father; ?>" data-mother="<?php echo $mother; ?>" data-showy="show" data-showl="show" data-showf="show" data-showm="show">
						<div class="row">
							<div class="col-lg-12 category-title">
								<?php echo $item->title; ?>
							</div>
							<div class="col-lg-12 category-description">
								<?php echo $item->description; ?>
							</div>




							<div class="col-lg-12 category-articles">
								<div class="row">
									<?php $category_articles = GetCategoryArticles($item->id); ?>
									<?php foreach ($category_articles as $article): ?>

										<div class="col-lg-3 article-item">
											<div class="article-title">
												<?php echo $article->title; ?>
											</div>
											<div>
												<?php
												$image = json_decode($article->images);
												$images_url = $image->image_intro;
												$images_alt = $image->image_intro_alt;
												echo '<img src="'.$images_url.'" alt="'.$images_alt.'" class="img-responsive">';
												?>
											</div>
											<div>
												<?php 
												$date = new DateTime($article->publish_up);
												echo $date->format('Y m d'); 
												?>
											</div>
											<div>

												<?php 

												$customFields = getCustomFields($article->id);
												foreach ($customFields as $field) { 
													if ( $field->field_id == $status_field_id ) {
														$fieldvalue = getCustomFieldsValue($status_field_id);
														$fieldparams = json_decode($fieldvalue[0]->fieldparams);
														foreach ($fieldparams->options as $name ) {
															if ( $name->value == $field->value ) {
																echo $name->name."<br>";
															}
														}
														continue;
													}
													if ( $field->field_id == $gender_field_id ) {
														$fieldvalue = getCustomFieldsValue($gender_field_id);
														$fieldparams = json_decode($fieldvalue[0]->fieldparams);
														foreach ($fieldparams->options as $name ) {
															if ( $name->value == $field->value ) {
																echo $name->name."<br>";
															}
														}
														continue;
													}
													if ( $field->field_id == $color_field_id ) {
														echo $field->value."<br>";
														continue;
													}
													if ( $field->field_id == $color_code_field_id ) {
														echo $field->value."<br>";
														continue;
													}
													if ( $field->field_id == $country_field_id ) {
														echo $field->value."<br>";
														continue;
													}
												} 

												?>

											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>