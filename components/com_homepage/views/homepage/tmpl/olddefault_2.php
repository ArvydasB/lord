<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_homepage
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

$select_category = JRequest::getVar('mycategory');

$avaivable_text = JRequest::getVar('avaivable_text');
$interesting_text = JRequest::getVar('interesting_text');
$reserved_text = JRequest::getVar('reserved_text');
$sold_text = JRequest::getVar('sold_text');
$stay_text = JRequest::getVar('stay_text');

$status_title = JRequest::getVar('status_title');
$gender_title = JRequest::getVar('gender_title');
$color_title = JRequest::getVar('color_title');
$birth_title = JRequest::getVar('birth_title');

$male_title = JRequest::getVar('male_title');
$female_title = JRequest::getVar('female_title');

$read_more_text = JRequest::getVar('read_more_text');

$no_left_kittens = JRequest::getVar('no_left_kittens');

$more_kittens_menu = JRequest::getVar('more_kittens_menu');
$more_kittens_button = JRequest::getVar('more_kittens_button');

$our_plans_menu = JRequest::getVar('our_plans_menu');
$up_plans_button = JRequest::getVar('up_plans_button');

$old_kittens_menu = JRequest::getVar('old_kittens_menu');
$old_kitten_button = JRequest::getVar('old_kitten_button');

$color_field_id = JRequest::getVar('color_field_id');
$color_code_field_id = JRequest::getVar('color_code_field_id');
$status_field_id = JRequest::getVar('status_field_id');
$gender_field_id = JRequest::getVar('gender_field_id');
$country_field_id = JRequest::getVar('country_field_id');

function getMaincats($main_id) {
	$db = JFactory::getDbo();
	$query_cat = $db->getQuery(true);
	$query_cat
	->select('#__categories.*')
	->from($db->quoteName('#__categories'))
	->where($db->quoteName('#__categories.parent_id') . ' = ' . $db->quote($main_id))
	->group($db->quoteName('#__categories.id') . ' ASC');
	$db->setQuery($query_cat);
	$all_cats = $db->loadObjectList();
	return $all_cats;
}
function getArticles($id) {
	$db = JFactory::getDbo();
	$query_cat = $db->getQuery(true);
	$query_cat
	->select('#__content.*')
	->from($db->quoteName('#__content'))
	->where($db->quoteName('#__content.catid') . ' = ' . $db->quote($id) . ' AND ' . $db->quoteName('#__content.featured') . ' = ' . $db->quote("1"));
	$db->setQuery($query_cat);
	$results = $db->loadObjectList();
	return $results;
}
function getCustomFields($id) {
	$db = JFactory::getDbo();
	$query_cat = $db->getQuery(true);
	$query_cat
	->select('#__fields_values.*')
	->from($db->quoteName('#__fields_values'))
	->where($db->quoteName('#__fields_values.item_id') . ' = ' . $db->quote($id));
	$db->setQuery($query_cat);
	$results = $db->loadObjectList();
	return $results;
}
function getCustomFieldsValue($field_id) {
	$db = JFactory::getDbo();
	$query_cat = $db->getQuery(true);
	$query_cat
	->select('#__fields.*')
	->from($db->quoteName('#__fields'))
	->where($db->quoteName('#__fields.id') . ' = ' . $db->quote($field_id));
	$db->setQuery($query_cat);
	$results = $db->loadObjectList();
	return $results;
}
?>
<div id="avaivable-kittens" class="container-fluid">
	<div id="content" class="container">
		<?php
		$document = JFactory::getDocument();
		$renderer   = $document->loadRenderer('modules');
		$position   = 'content-home-avaivable';
		$options   = array('style' => 'raw');
		echo $renderer->render($position, $options, null);
		?>
	</div>
	<div id="kittens" class="container">
		<?php 
		$all_category = getMaincats($select_category);
		foreach ($all_category as $category): ?>
		<?php 
		$category_items = getArticles($category->id);
		if ( count($category_items) > 0 ): ?>
		<div class="row">
			<?php $i = 0; ?>
			<?php shuffle($category_items); ?>
			<?php foreach ($category_items as $item): ?>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
					<div>
						<?php echo $item->title; ?>
					</div>
					<div>
						<?php
						$images = json_decode($item->images);
						$images_url = $images->image_intro;
						$images_alt = $images->image_intro_alt;
						echo '<img src="'.$images_url.'" alt="'.$images_alt.'" class="img-responsive">';
						?>
					</div>
					<div>
						<?php 
						$date = new DateTime($item->publish_up);
						echo $date->format('Y m d'); 
						?>
					</div>
					<?php 
					$customFields = getCustomFields($item->id);
					foreach ($customFields as $field) { 
						if ( $field->field_id == $status_field_id ) {
							$fieldvalue = getCustomFieldsValue($status_field_id);
							$fieldparams = json_decode($fieldvalue[0]->fieldparams);
							foreach ($fieldparams->options as $name ) {
								if ( $name->value == $field->value ) {
									echo $name->name."<br>";
								}
							}
							continue;
						}
						if ( $field->field_id == $gender_field_id ) {
							$fieldvalue = getCustomFieldsValue($gender_field_id);
							$fieldparams = json_decode($fieldvalue[0]->fieldparams);
							foreach ($fieldparams->options as $name ) {
								if ( $name->value == $field->value ) {
									echo $name->name."<br>";
								}
							}
							continue;
						}
						if ( $field->field_id == $color_field_id ) {
							echo $field->value."<br>";
							continue;
						}
						if ( $field->field_id == $color_code_field_id ) {
							echo $field->value."<br>";
							continue;
						}
						if ( $field->field_id == $country_field_id ) {
							echo $field->value."<br>";
							continue;
						}
					} 
					$item_url = trim( JURI::base(), '/' ) . JRoute::_( ContentHelperRoute::getArticleRoute($item->id, $item->catid) );
					?>
					<a href="<?php echo $item_url; ?>"><?php echo $read_more_text; ?></a>
				</div>
				<?php if(++$i > 2) break; ?>
			<?php endforeach; ?>
		</div>
		<div class="">
			<a href="index.php?Itemid=<?php echo $more_kittens_menu; ?>"><?php echo $more_kittens_button; ?></a>
		</div>
	<?php else: ?>
		<div class="">
			<?php echo $no_left_kittens; ?>
		</div>
		<div class="">
			<a href="index.php?Itemid=<?php echo $old_kittens_menu; ?>"><?php echo $old_kitten_button; ?></a>
		</div>
		<div class="">
			<a href="index.php?Itemid=<?php echo $our_plans_menu; ?>"><?php echo $up_plans_button; ?></a>
		</div>
	<?php endif; ?>
<?php endforeach; ?>
</div>
</div>
