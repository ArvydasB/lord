<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_homepage
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

$select_category = JRequest::getVar('mycategory');

$avaivable_text = JRequest::getVar('avaivable_text');
$interesting_text = JRequest::getVar('interesting_text');
$reserved_text = JRequest::getVar('reserved_text');
$sold_text = JRequest::getVar('sold_text');
$stay_text = JRequest::getVar('stay_text');

$status_title = JRequest::getVar('status_title');
$gender_title = JRequest::getVar('gender_title');
$color_title = JRequest::getVar('color_title');
$birth_title = JRequest::getVar('birth_title');

$male_title = JRequest::getVar('male_title');
$female_title = JRequest::getVar('female_title');

$read_more_text = JRequest::getVar('read_more_text');

$no_left_kittens = JRequest::getVar('no_left_kittens');

$more_kittens_menu = JRequest::getVar('more_kittens_menu');
$more_kittens_button = JRequest::getVar('more_kittens_button');

$our_plans_menu = JRequest::getVar('our_plans_menu');
$up_plans_button = JRequest::getVar('up_plans_button');

$old_kittens_menu = JRequest::getVar('old_kittens_menu');
$old_kitten_button = JRequest::getVar('old_kitten_button');

if (!function_exists('getMaincats')) {
	function getMaincats($main_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__categories.*')
		->from($db->quoteName('#__categories'))
		->where($db->quoteName('#__categories.parent_id') . ' = ' . $db->quote($main_id))
		->group($db->quoteName('#__categories.id') . ' ASC');
		$db->setQuery($query_cat);
		$all_cats = $db->loadObjectList();
		return $all_cats;
	}
}
if (!function_exists('getArticles')) {
	function getArticles($id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__content.*')
		->from($db->quoteName('#__content'))
		->where($db->quoteName('#__content.catid') . ' = ' . $db->quote($id) . ' AND ' . $db->quoteName('#__content.featured') . ' = ' . $db->quote("1"));
		$db->setQuery($query_cat);
		$results = $db->loadObjectList();
		return $results;
	}
}
?>

<div id="avaivable-kittens" class="container-fluid">


	<div id="content" class="container">

		<?php
		$document = JFactory::getDocument();
		$renderer   = $document->loadRenderer('modules');
		$position   = 'content-home-avaivable';
		$options   = array('style' => 'raw');
		echo $renderer->render($position, $options, null);
		?>

	</div>
	<div id="kittens" class="container">

		<?php $all_category = getMaincats($select_category); ?>
		<?php foreach ($all_category as $category): ?>


			


		<?php endforeach; ?>


	</div>
</div>
