<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_homepage
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

$select_category = JRequest::getVar('mycategory');

$avaivable_text = JRequest::getVar('avaivable_text');
$interesting_text = JRequest::getVar('interesting_text');
$reserved_text = JRequest::getVar('reserved_text');
$sold_text = JRequest::getVar('sold_text');
$stay_text = JRequest::getVar('stay_text');

$status_title = JRequest::getVar('status_title');
$gender_title = JRequest::getVar('gender_title');
$color_title = JRequest::getVar('color_title');
$birth_title = JRequest::getVar('birth_title');

$male_title = JRequest::getVar('male_title');
$female_title = JRequest::getVar('female_title');

$read_more_text = JRequest::getVar('read_more_text');

$no_left_kittens = JRequest::getVar('no_left_kittens');

$more_kittens_menu = JRequest::getVar('more_kittens_menu');
$more_kittens_button = JRequest::getVar('more_kittens_button');

$our_plans_menu = JRequest::getVar('our_plans_menu');
$up_plans_button = JRequest::getVar('up_plans_button');

$old_kittens_menu = JRequest::getVar('old_kittens_menu');
$old_kitten_button = JRequest::getVar('old_kitten_button');

$color_field_id = JRequest::getVar('color_field_id');
$color_code_field_id = JRequest::getVar('color_code_field_id');
$status_field_id = JRequest::getVar('status_field_id');
$gender_field_id = JRequest::getVar('gender_field_id');
$country_field_id = JRequest::getVar('country_field_id');


if (!function_exists('GetCategoryWithFeaturedArticle')) {
	function GetCategoryWithFeaturedArticle($main_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__content.*')
		->from($db->quoteName('#__content'))
		->join('LEFT', $db->quoteName('#__categories') . ' ON (' . $db->quoteName('#__content.catid') . ' = ' . $db->quoteName('#__categories.id') . ')')
		->where($db->quoteName('#__categories.parent_id') . ' = ' . $db->quote($main_id), 'AND')
		->where($db->quoteName('#__content.featured') . ' = 1 ' );
		$db->setQuery($query_cat);
		$all_cats = $db->loadObjectList();
		return $all_cats;
	}
}
if (!function_exists('getCustomFields')) {
	function getCustomFields($id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__fields_values.*')
		->from($db->quoteName('#__fields_values'))
		->where($db->quoteName('#__fields_values.item_id') . ' = ' . $db->quote($id));
		$db->setQuery($query_cat);
		$results = $db->loadObjectList();
		return $results;
	}
}
if (!function_exists('getCustomFieldsValue')) {
	function getCustomFieldsValue($field_id) {
		$db = JFactory::getDbo();
		$query_cat = $db->getQuery(true);
		$query_cat
		->clear()
		->select('#__fields.*')
		->from($db->quoteName('#__fields'))
		->where($db->quoteName('#__fields.id') . ' = ' . $db->quote($field_id));
		$db->setQuery($query_cat);
		$results = $db->loadObjectList();
		return $results;
	}
}



?>
<div id="avaivable-kittens" class="container-fluid">
	<div id="content" class="container">
		<?php
		$document = JFactory::getDocument();
		$renderer   = $document->loadRenderer('modules');
		$position   = 'content-home-avaivable';
		$options   = array('style' => 'raw');
		echo $renderer->render($position, $options, null);
		?>
	</div>
	<div id="kittens" class="container">
		<div class="row">
			<?php 
			$all_items = GetCategoryWithFeaturedArticle($select_category);
			if ((int)$all_items > 0) {
				$i = 0;
				shuffle($all_items); 
				?>


				<?php 

				for ($i=0; $i < 3 ; $i++) { 
					$title = $all_items[$i]->title;
					$date = new DateTime($all_items[$i]->publish_up);
					$images = json_decode($all_items[$i]->images);
					$images_url = $images->image_intro;
					$images_alt = $images->image_intro_alt;
					$item_url = trim( JURI::base(), '/' ) . JRoute::_( ContentHelperRoute::getArticleRoute($all_items[$i]->id, $all_items[$i]->catid) );
					$customFields = getCustomFields($all_items[$i]->id);
					for ($a=0; $a < count($customFields); $a++) { 
						if (  $customFields[$a]->field_id == $status_field_id ) {
							$fieldvalue = getCustomFieldsValue($status_field_id);
							$fieldparams = json_decode($fieldvalue[0]->fieldparams);
							$fieldoption = $fieldparams->options;
							foreach ($fieldoption as $name ) {
								if ( $name->value == $customFields[$a]->value ) {
									$item_status = $name->name;
								}
							}
						}
						if (  $customFields[$a]->field_id == $gender_field_id ) {
							$fieldvalue = getCustomFieldsValue($gender_field_id);
							$fieldparams = json_decode($fieldvalue[0]->fieldparams);
							$fieldoption = $fieldparams->options;
							foreach ($fieldoption as $name ) {
								if ( $name->value == $customFields[$a]->value ) {
									$item_gender = $name->name;
								}
							}
						}
						if (  $customFields[$a]->field_id == $color_field_id ) {
							$item_color = $customFields[$a]->value;
						}
						if (  $customFields[$a]->field_id == $color_code_field_id ) {
							$item_color_code = $customFields[$a]->value;
						}	
					}
					?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 article-item">
						<div class="item-block">
							<div class="article-image">
								<a href="<?php echo $item_url; ?>">
									<?php echo '<img src="'.$images_url.'" alt="'.$images_alt.'" class="img-responsive">'; ?>
								</a>
							</div>
							<div class="item-info">
								<div class="item-title"><a href="<?php echo $item_url; ?>"><?php echo $title; ?></a></div>
								<div class="item-date"><?php echo $birth_title .': '; ?><span><?php echo $date->format('Y m d'); ?></span></div>
								<div class="more-info">
									<div class="item-color"><?php echo $color_title .': '; ?><a type="button" data-toggle="tooltip" data-placement="right" title="<?php echo $item_color_code; ?>"><?php echo $item_color; ?></a></div>
									<div class="item-gender"><?php echo $gender_title .': '; ?><span><?php echo $item_gender; ?></span></div>
									<div class="item-status"><?php echo $status_title .': '; ?><span><?php echo $item_status; ?></span></div>
									<div class="item-readmore">
										<a href="<?php echo $item_url; ?>"><?php echo $read_more_text; ?></a>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<?php
				}
				?>
			</div>
 			<div class="more-kittens-bottom col-lg-12">
				<a href="index.php?Itemid=<?php echo $more_kittens_menu; ?>"><?php echo $more_kittens_button; ?></a>
			</div>
			<?php 
		} else {
			?>
		</div>
		<div class="no-left-kittens-bottom col-lg-12">
			<p><?php echo $no_left_kittens; ?></p>
			<a href="index.php?Itemid=<?php echo $our_plans_menu; ?>"><?php echo $up_plans_button; ?></a>
			<a href="index.php?Itemid=<?php echo $old_kittens_menu; ?>"><?php echo $old_kitten_button; ?></a>
		</div>
		<?php
	}
	?>
</div>
</div>
