<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_homepage
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

if(!class_exists('ContentHelperRoute')) require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

$select_category = JRequest::getVar('category_id');

$male_title = JRequest::getVar('male_title');
$female_title = JRequest::getVar('female_title');

$read_more_text = JRequest::getVar('read_more_text');


function getArticles($id) {
	$db = JFactory::getDbo();
	$query_cat = $db->getQuery(true);
	$query_cat
	->select('#__content.*')
	->from($db->quoteName('#__content'))
	->where($db->quoteName('#__content.catid') . ' = ' . $db->quote($id));
	$db->setQuery($query_cat);
	$results = $db->loadObjectList();
	return $results;
}

?>

<div id="content" class="container">

	<div class="row">

		<?php foreach (getArticles($select_category) as $item) { ?>

		<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 item">

			<?php echo $item->title; ?>

			<?php
			$images = json_decode($item->images);
			$images_url = $images->image_intro;
			$images_alt = $images->image_intro_alt;
			echo '<img src="'.$images_url.'" alt="'.$images_alt.'" class="img-responsive">';
			?>

			<?php 
			$date = new DateTime($item->publish_up);
			echo $date->format('Y m d'); 
			?>


			<?php
			$item_url = trim( JURI::base(), '/' ) . JRoute::_( ContentHelperRoute::getArticleRoute($item->id, $item->catid) );
			?>

			<a href="<?php echo $item_url; ?>"><?php echo $read_more_text; ?></a>

		</div>

		<?php } ?>

	</div>

</div>
