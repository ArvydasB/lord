<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
jQuery(window).load(function()
{
var igalleryMenu<?php echo $this->uniqueid; ?> = new igalleryMenuClass({uniqueid: <?php echo '"'.$this->uniqueid.'"'; ?>,menuGridType: <?php echo '"'.$this->profile->menu_grid_type.'"'; ?>,menuHoverEffect: <?php echo '"'.$this->profile->menu_hover_effect.'"'; ?>});
});
</script>

<div class="igallery_clear"></div>
<form action="index.php?option=com_igallery&amp;view=category&amp;id=<?php echo $this->category->id; ?>&amp;Itemid=<?php echo $this->Itemid; ?>" method="post" name="ig_menu_pagination" class="ig_menu_pagination" id="ig_menu_pagination<?php echo $this->uniqueid; ?>">

<?php $allCategories = igStaticHelper::getCategories(); ?>

<?php if(count($this->categoryChildren) != 0): ?>

	<div id="cat_child_wrapper<?php echo $this->uniqueid; ?>" class="cat_child_wrapper cat_child_wrapper_<?php echo $this->profile->style; ?> profile<?php echo $this->profile->id; ?>" data-maxmenuwidth="<?php echo $this->profile->menu_max_width; ?>">
	<?php $counter = 0; ?>
	
	<?php while( $counter < count($this->categoryChildren) ): ?>

        <?php if( isset($this->categoryChildren[$counter]) ):
            $row = $this->categoryChildren[$counter];
            $linkItemid = $this->source == 'component' ? $this->Itemid : igUtilityHelper::getItemid($row->id);
            $link = JRoute::_('index.php?option=com_igallery&amp;view=category&amp;igid='.$row->id.'&amp;Itemid='.$linkItemid);
            $divStyle = isset($row->fileArray) ? 'max-width:'.($row->fileArray['width'] + 6).'px;' : 'width:'.$this->profile->menu_max_width.'px;';
            $overlayClass = $this->profile->menu_text_position == 'overlay' && isset($row->fileArray)  ? 'overlay' : 'notoverlay';
            ?>

            <div class="cat_child" style="<?php echo $divStyle; ?>" >
                <div class="cat_child_inner">

                <?php if( isset($row->fileArray)): ?>
                    <?php $linkStyle = $this->profile->menu_image_align == 'center' ? 'display:block; width:'.($row->fileArray['width'] + 6).'px; height:'.($row->fileArray['height'] + 6).'px;' : ''; ?>
                    <a class="cat_child_img_a" href="<?php echo $link; ?>" style="<?php echo $linkStyle; ?>">
                       <img class="cat_child_img" src="<?php echo IG_IMAGE_HTML_RESIZE; ?><?php echo $row->fileArray['folderName']; ?>/<?php echo $row->fileArray['fullFileName']; ?>" alt="<?php echo $row->name; ?>" />
                    </a>
                <?php endif; ?>

                <div class="cat_child_text cat_child_text_<?php echo $overlayClass; ?>">

                    <a href="<?php echo $link; ?>" class="cat_child_a">
                    <div class="cat_child_background cat_child_background_<?php echo $overlayClass; ?>">

                        <<?php echo $this->profile->menu_heading_type; ?> class="cat_child_h3 cat_child_heading cat_child_heading_<?php echo $overlayClass; ?>">
                            <?php echo $row->name; ?>
                        </<?php echo $this->profile->menu_heading_type; ?>>

                        <div class="cat_child_des"><?php echo $row->menu_description; ?></div>

                        <?php if($this->profile->show_cat_author == 1): ?>
                            <?php $authorText = $this->params->get('show_name_username', 'name') == 'name' ? $row->displayname : $row->username; ?>
                            <div class="menu_text_block menu_text_block_author"><?php echo JText::_('JAUTHOR'); ?>: <?php echo $authorText; ?></div>
                        <?php endif; ?>

                        <?php if($this->profile->show_image_count == 1 ): ?>
                            <?php if($row->numimages > 0): ?>
                                <div class="menu_text_block menu_text_block_num"><?php echo $row->numimages; ?> <?php echo JText::_('IMAGES'); ?></div>
                            <?php else: ?>
                                <?php $childCategories = igTreeHelper::getChildIds($allCategories, $row->id); ?>
                                <?php if( count($childCategories) > 0 ): ?>
                                    <?php $categoryText = count($childCategories) > 1 ? JText::_('COM_IGALLERY_CATEGORIES') : JText::_('JCATEGORY'); ?>
                                    <div class="menu_text_block menu_text_block_child"><?php echo count($childCategories); ?> <?php echo $categoryText; ?></div>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        <?php if($this->profile->show_category_hits == 1): ?>
                        <div class="menu_text_block menu_text_block_hits"><?php echo $row->hits; ?> <?php echo JText::_('COM_IGALLERY_VIEWS'); ?></div>
                        <?php endif; ?>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php $counter++; ?>

    <?php endwhile; ?>
<div class="igallery_clear"></div>
</div>
<div class="igallery_clear"></div>
<?php endif; ?>

<?php if($this->profile->menu_pagination == 1): ?>

    <?php if($this->menuPagination->total > $this->menuPagination->limit): ?>
        <div class="pagination">
            <?php echo $this->menuPagination->getPagesLinks(); ?>
        </div>

        <div class="pagination">
            <?php echo $this->menuPagination->getPagesCounter(); ?>
        </div>
    <?php endif; ?>

<?php endif; ?>

</form>
<div class="igallery_clear"></div>