<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php $this->mode = 'main'; ?>
<?php $this->mobilePrefix = ($this->profile->mobile) ? 'mobile' : 'notmobile'; ?>

<?php if(JRequest::getInt('ig_been_called', 0) == 0): ?>
<div id="ig_jquery_warning" style="display: none;"></div>
<script type="text/javascript">
if(document.addEventListener){document.addEventListener('DOMContentLoaded', function(){if(typeof jQuery == 'undefined'){
document.getElementById('ig_jquery_warning').style.display = 'block';document.getElementById('ig_jquery_warning').style.color = 'red';
<?php if(IG_J30): ?>document.getElementById('ig_jquery_warning').innerHTML = 'Gallery Error: the javascript library Jquery has not been included, please check your template options for including Jquery';
<?php else: ?>document.getElementById('ig_jquery_warning').innerHTML = 'Gallery Error: the javascript library Jquery has not been included, please check your template options for including Jquery, or go to the gallery component options -> general tab to include Jquery';<?php endif; ?>}}, false);}
</script>
<?php JRequest::setVar('ig_been_called', 1); ?>
<?php endif; ?>

<?php if(isset($this->headJs)): ?>
<script type="text/javascript">
<?php echo $this->headJs; ?>
</script>
<?php endif; ?>

    <?php if($this->type == 'category'): ?>
    <?php echo $this->loadTemplate('header'); ?>
<?php endif; ?>

<?php if($this->profile->show_search == 1): ?>
    <?php echo $this->loadTemplate('search'); ?>
<?php endif; ?>

<?php if($this->profile->gallery_des_position == 'above' && strlen($this->category->gallery_description) > 1 && $this->type == 'category'): ?>
    <?php echo $this->loadTemplate('category_description'); ?>
<?php endif; ?>

<?php if( count($this->categoryChildren) && JRequest::getInt('ighidemenu', 0) != 1 ): ?>
    <?php echo $this->loadTemplate('menu'); ?>
<?php endif; ?>

<?php if( !empty($this->photoList) ): ?>


    <?php $maxWidth = ($this->profile->thumb_layout == 'grid' && $this->profile->show_thumbs == 1 && $this->profile->show_large_image == 0) ? '' : 'max-width: '.$this->dimensions['galleryWidth'].'px;'; ?>
    <?php $align = $this->profile->align == 'center' ? 'margin-left: auto; margin-right: auto;' : 'float: '.$this->profile->align; ?>
	<a id="gallery-<?php echo $this->uniqueid; ?>" style="height: 0px!important; visibility: hidden;"></a>
    <div id="main_images_wrapper<?php echo $this->uniqueid; ?>" class="main_images_wrapper main_images_wrapper_<?php echo $this->profile->style; ?> profile<?php echo $this->profile->id; ?>" style="<?php echo $maxWidth; ?> <?php echo $align; ?>" >

    <?php if($this->profile->thumb_position == 'above' && $this->profile->thumb_layout == 'scroller' && $this->profile->show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->photo_des_position == 'above' && $this->profile->show_descriptions == 1 && $this->profile->show_large_image == 1 && $this->desVars->mainHasDescriptions == true) : ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->photo_des_position == 'left' && $this->profile->show_descriptions == 1  && $this->profile->show_large_image == 1 && $this->desVars->mainHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->thumb_position == 'left' && $this->profile->thumb_layout == 'scroller' && $this->profile->show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->show_large_image == 1): ?>

        <?php $slideshow_width_percent = round( ($this->dimensions['mainSlideShowDivWidth']/$this->dimensions['galleryWidth']) * 100, 2); ?>

        <?php $centerAlignClass = $this->profile->main_image_align_horiz == 'center' ? 'main_large_image_center' : ''; ?>
        <?php $rightAlignClass = $this->profile->main_image_align_horiz == 'right' ? 'slick_large_img_right' : ''; ?>

	    <div id="main_image_slideshow_wrapper<?php echo $this->uniqueid; ?>" class="main_image_slideshow_wrapper" style="width: <?php echo $slideshow_width_percent; ?>%;">

        <div id="main_large_image<?php echo $this->uniqueid; ?>" class="main_large_image <?php echo $centerAlignClass; ?>" style="width: <?php echo round( ($this->dimensions['mainImageDivWidth']/$this->dimensions['mainSlideShowDivWidth']) * 100, 2); ?>%;" >

            <?php for($i=0; $i<count($this->photoList); $i++): ?>

                <?php $imgPath = IG_IMAGE_HTML_RESIZE.$this->mainFiles[$i]['folderName'].'/'.$this->mainFiles[$i]['fullFileName']; ?>

                <?php $srcAtt = $this->activeImage == $i ? 'src="'.$imgPath.'"' : 'src="'.IG_IMAGE_ASSET_PATH.'1px-placeholder.png" data-ig-lazy-src="'.$imgPath.'"'; ?>
                <?php $isFirstClass = $this->activeImage == $i ? 'firstImg' : 'notFirstImg'; ?>

                <div class="ig-slick-image" style="display:none;">

                <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                    <div class="main_image_overlay_wrapper">
                <?php endif; ?>

                <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                        <img <?php echo $srcAtt; ?> title="<?php echo $this->photoList[$i]->alt_text; ?>" alt="<?php echo $this->photoList[$i]->alt_text; ?>" class="slick_large_img large_img <?php echo $isFirstClass; ?> <?php echo $rightAlignClass; ?>"/>
                    <?php else: ?>
                        <?php echo $this->photoList[$i]->embed_code; ?>
                    <?php endif; ?>

                    <?php if($this->profile->photo_des_position == 'overlay' && $this->profile->show_descriptions == 1 && $this->desVars->mainHasDescriptions == true && empty($this->photoList[$i]->embed_code)): ?>
                        <?php $imgFileName = igUtilityHelper::getDescriptionText($this->profile->show_filename,$this->photoList[$i]); ?>
                        <div class="des_div_overlay_outer des_div_overlay_outer_<?php echo $this->profile->style; ?>"><div class="des_div_overlay_inner"><?php if( strlen($imgFileName) > 1): ?><span class="des_filename"><?php echo $imgFileName;?> </span><?php endif; ?><?php echo $this->photoList[$i]->description; ?></div></div>

                    <?php endif; ?>

                    <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                    </div>
                    <?php endif; ?>

                </div>
            <?php endfor; ?>
        </div>

		<div class="igallery_clear"></div>


		<?php if($this->profile->show_slideshow_controls == 1 && $this->profile->slideshow_position == 'below'): ?>
			<div id="main_slideshow_buttons<?php echo $this->uniqueid; ?>" class="main_slideshow_buttons" >
				<div id="slideshow_rewind<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_rewind" ></div>
				<div id="slideshow_play<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_play" ></div>
				<div id="slideshow_forward<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_forward" ></div>
			</div>
		<?php endif; ?>

		<?php if($this->profile->allow_rating == 2): ?>
			<?php echo $this->loadTemplate('ratings'); ?>
		<?php endif; ?>

		<?php if($this->profile->download_image != 'none'): ?>
		   <div id="main_download_button<?php echo $this->uniqueid; ?>" class="main_download_button" >
			   <a href="#">

			   </a>
		   </div>
		<?php endif; ?>

		<?php if($this->profile->share_facebook == 1): ?>
			<div id="main_facebook_share<?php echo $this->uniqueid; ?>" class="main_facebook_share"></div>
        <?php endif; ?>

		<?php if($this->profile->plus_one == 1): ?>
			<div id="main_plus_one_div<?php echo $this->uniqueid; ?>" class="main_plus_one_div"></div>
		<?php endif; ?>

		<?php if($this->profile->twitter_button == 1): ?>
			<div id="main_twitter_button<?php echo $this->uniqueid; ?>" class="main_twitter_button"></div>
		<?php endif; ?>

        <?php if($this->profile->pinterest_button == 1): ?>
            <div id="main_pinterest<?php echo $this->uniqueid; ?>" class="main_pinterest"></div>
        <?php endif; ?>

        <?php if($this->profile->show_image_author == 1): ?>
            <div id="main_image_author<?php echo $this->uniqueid; ?>" class="main_image_author" >
                <?php echo JText::_( 'JAUTHOR' ) ?>: <span class="main_image_author_name" ></span>
            </div>
        <?php endif; ?>

        <?php if($this->profile->show_image_hits == 1): ?>
            <div id="main_image_hits<?php echo $this->uniqueid; ?>" class="main_image_hits" >
                <span class="main_image_hits_count" ></span> <?php echo JText::_('COM_IGALLERY_VIEWS'); ?>
            </div>
        <?php endif; ?>

        <?php if($this->profile->show_numbering == 1): ?>
            <div id="main_img_numbering<?php echo $this->uniqueid; ?>" class="main_img_numbering">
                <span id="main_image_number<?php echo $this->uniqueid; ?>"></span>&#47;<?php echo count($this->photoList); ?>
            </div>
        <?php endif; ?>

		<?php if($this->profile->report_image == 1): ?>
			<div id="main_report<?php echo $this->uniqueid; ?>" class="main_report" >
			   <a href="#"><?php echo JText::_( 'REPORT_IMAGE' ) ?></a>
			   <form action="#" method="post" name="main_report_form" id="main_report_form" style="display: none;">
					<textarea name="report_textarea" id="main_report_textarea<?php echo $this->uniqueid; ?>" rows="4" style="width: 100%;"></textarea>
					<input name="Itemid" type="hidden" value="<?php echo JRequest::getInt('Itemid'); ?>" />
					<input type="submit" value="<?php echo JText::_( 'JSUBMIT' ) ?>" />
				</form>
			</div>
		<?php endif; ?>

		</div>
		<?php endif; ?>


    <?php if($this->profile->thumb_position == 'right' && $this->profile->thumb_layout == 'scroller' && $this->profile->show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->photo_des_position == 'right' && $this->profile->show_descriptions == 1 && $this->profile->show_large_image == 1  && $this->desVars->mainHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <div class="igallery_clear"></div>
    <?php if($this->profile->photo_des_position == 'below' && $this->profile->show_descriptions == 1  && $this->profile->show_large_image == 1  && $this->desVars->mainHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->show_tags == 1): ?>
        <?php echo $this->loadTemplate('tags'); ?>
    <?php endif; ?>

    <?php if($this->profile->thumb_position == 'below' && $this->profile->thumb_layout == 'scroller' && $this->profile->show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->thumb_layout == 'grid' && $this->profile->show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs_grid'); ?>
    <?php endif; ?>

    <?php if($this->profile->thumb_pagination == 1): ?>
        <div class="igallery_clear"></div>
        <form action="index.php?option=com_igallery&amp;view=igcategory&amp;id=<?php echo $this->category->id; ?>&amp;Itemid=<?php echo $this->Itemid; ?>" method="post" name="ig_thumb_pagination">
            <div class="pagination">
                <?php echo $this->thumbPagination->getPagesLinks(); ?>
            </div>
        </form>
    <?php endif; ?>

    <?php if($this->profile->allow_comments == 2): ?>
        <?php echo $this->loadTemplate('jcomments'); ?>
    <?php endif; ?>

    <?php if($this->profile->allow_comments == 4 || $this->profile->lbox_allow_comments == 4 || $this->profile->share_facebook == 1 || $this->profile->lbox_share_facebook == 1): ?>
        <div id="fb-root"></div>
        <script type="text/javascript">
        // <![CDATA[
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) {return;}
          js = d.createElement(s); js.id = id;
          js.src = "https://connect.facebook.net/<?php echo str_replace('-', '_', $this->languageTag); ?>/all.js#xfbml=1<?php if( strlen( $this->params->get('fb_comments_appid', '') ) > 1 ): ?>&appId=<?php echo $this->params->get('fb_comments_appid', ''); ?><?php endif; ?>";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        //]]>
        </script>
    <?php endif; ?>

    <?php if($this->profile->allow_comments == 4 && $this->profile->comments_position == 'below'): ?>
        <?php echo $this->loadTemplate('fbcomments'); ?>
    <?php endif; ?>

    </div>
<?php endif; ?>

<?php if($this->profile->gallery_des_position == 'below' && strlen($this->category->gallery_description) > 1 ): ?>
<div class="igallery_clear"></div>
	<?php echo $this->loadTemplate('category_description'); ?>
<?php endif; ?>

<?php if($this->profile->lightbox == 1 && !empty($this->photoList) ): ?>
    <?php echo $this->loadTemplate('lightbox'); ?>
<?php endif; ?>

<?php if( !empty($this->photoList) ): ?>
<div class="big_image_data" style="display: none;">

    <?php for($i=0; $i<count($this->photoList); $i++): ?>

        <?php $linkText = empty($this->photoList[$i]->alt_text) ? $this->photoList[$i]->filename : $this->photoList[$i]->alt_text; ?>

        <?php if($this->profile->lightbox == 1): ?>
            <a href="<?php echo IG_IMAGE_HTML_RESIZE.$this->lboxFiles[$i]['folderName'].'/'.$this->lboxFiles[$i]['fullFileName']; ?>"><?php echo $linkText; ?></a>
        <?php else: ?>
            <a href="<?php echo IG_IMAGE_HTML_RESIZE.$this->mainFiles[$i]['folderName'].'/'.$this->mainFiles[$i]['fullFileName']; ?>"><?php echo $linkText; ?></a>
        <?php endif; ?>

    <?php endfor; ?>
</div>
<?php endif; ?>

<div class="igallery_clear"></div>


