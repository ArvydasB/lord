<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

<?php
$this->mode = 'lbox';
$whiteIconClass = $this->profile->lbox_back_color == 'black' && $this->profile->lbox_front_color == 'none' ? 'lbox_white_icons' : '';

if($this->profile->lbox_back_color == 'white')
{
    $backColor = '#ffffff';
}
else if($this->profile->lbox_back_color == 'custom')
{
    $backColor = $this->profile->lbox_back_custom;
}
else if($this->profile->lbox_back_color == 'none')
{
    $backColor = '';
}
else
{
    $backColor = '#000000';
}

if($this->profile->lbox_front_color == 'black')
{
    $frontColor = '#000000';
}
else if($this->profile->lbox_front_color == 'custom')
{
    $frontColor = $this->profile->lbox_front_custom;
}
else if($this->profile->lbox_front_color == 'none')
{
    $frontColor = '';
}
else
{
    $frontColor = '#ffffff';
}

$mobileClass = $this->profile->phone ? 'phone' : 'not_phone';
?>

<div id="lbox_dark<?php echo $this->uniqueid; ?>" class="lbox_dark" style="display: none; <?php if( !empty($backColor) ): ?>background-color: <?php echo $backColor; ?> <?php endif; ?>" ></div>

<div id="lbox_white<?php echo $this->uniqueid; ?>" class="lbox_white lbox_white_<?php echo $this->profile->style; ?> profile<?php echo $this->profile->id; ?> <?php echo 'lbox_white_'.$mobileClass; ?> <?php echo $whiteIconClass; ?>" style="max-width:<?php echo $this->dimensions['galleryLboxWidth']; ?>px; display: none; <?php if( !empty($frontColor) ): ?>background-color: <?php echo $frontColor; ?> <?php endif; ?>" >

    <a id="closeImage<?php echo $this->uniqueid; ?>" class="closeImage closeImage_<?php echo $mobileClass; ?>"></a>

    <?php if($this->profile->lbox_thumb_position == 'above' && $this->profile->lbox_show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_photo_des_position == 'above' && $this->profile->lbox_show_descriptions == 1 && $this->desVars->lboxHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_photo_des_position == 'left' && $this->profile->lbox_show_descriptions == 1 && $this->desVars->lboxHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_thumb_position == 'left' && $this->profile->lbox_show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php $slideshow_width_percent = round( ($this->dimensions['lboxSlideShowDivWidth']/$this->dimensions['galleryLboxWidth']) * 100, 2); ?>
    <?php $centerAlignClass = $this->profile->lbox_image_align_horiz == 'center' ? 'main_large_image_center' : ''; ?>
    <?php $rightAlignClass = $this->profile->lbox_image_align_horiz == 'right' ? 'slick_large_img_right' : ''; ?>

    <div id="lbox_image_slideshow_wrapper<?php echo $this->uniqueid; ?>" class="lbox_image_slideshow_wrapper" style="width: <?php echo $slideshow_width_percent; ?>%;">

        <div id="lbox_large_image<?php echo $this->uniqueid; ?>" class="lbox_large_image <?php echo $centerAlignClass; ?>" style="width: <?php echo round( ($this->dimensions['lboxImageDivWidth']/$this->dimensions['lboxSlideShowDivWidth']) * 100, 2); ?>%;">

            <?php for($i=0; $i<count($this->photoList); $i++): ?>

                <?php $imgPath = IG_IMAGE_HTML_RESIZE.$this->lboxFiles[$i]['folderName'].'/'.$this->lboxFiles[$i]['fullFileName']; ?>

                <?php $srcAtt = $this->activeImage == $i ? 'src="'.$imgPath.'"' : 'src="'.IG_IMAGE_ASSET_PATH.'1px-placeholder.png" data-ig-lazy-src="'.$imgPath.'"'; ?>
                <?php $isFirstClass = $this->activeImage == $i ? 'firstImg' : 'notFirstImg'; ?>

                <div class="ig-slick-image">

                <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                    <div class="lbox_image_overlay_wrapper">
                <?php endif; ?>

                <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                <img <?php echo $srcAtt; ?> title="<?php echo $this->photoList[$i]->alt_text; ?>" alt="<?php echo $this->photoList[$i]->alt_text; ?>" class="slick_large_img large_img <?php echo $isFirstClass; ?> <?php echo $rightAlignClass; ?>"/>
                <?php else: ?>
                    <?php echo $this->photoList[$i]->embed_code; ?>
                <?php endif; ?>

                <?php if($this->profile->lbox_photo_des_position == 'overlay' && $this->profile->lbox_show_descriptions == 1 && $this->desVars->lboxHasDescriptions == true && empty($this->photoList[$i]->embed_code)): ?>
                    <?php $imgFileName = igUtilityHelper::getDescriptionText($this->profile->lbox_show_filename,$this->photoList[$i]); ?>
                    <div class="des_div_overlay_outer des_div_overlay_outer_<?php echo $this->profile->style; ?>"><div class="des_div_overlay_inner"><?php if( strlen($imgFileName) > 1): ?><span class="des_filename"><?php echo $imgFileName;?> </span><?php endif; ?><?php echo $this->photoList[$i]->description; ?></div></div>
                <?php endif; ?>

                <?php if( empty($this->photoList[$i]->embed_code) ): ?>
                </div>
                <?php endif; ?>

                </div>
            <?php endfor; ?>


        </div>
		

		
		<div class="igallery_clear"></div>

        <?php if($this->profile->lbox_show_slideshow_controls == 1 && $this->profile->lbox_slideshow_position == 'below'): ?>
			<div id="lbox_slideshow_buttons<?php echo $this->uniqueid; ?>" class="lbox_slideshow_buttons" >
				<div id="lbox_slideshow_rewind<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_rewind" /></div>
				<div id="lbox_slideshow_play<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_play"/></div>
				<div id="lbox_slideshow_forward<?php echo $this->uniqueid; ?>"  class="ig_slideshow_img ig_slideshow_forward" /></div>
			</div>
        <?php endif; ?>

        <?php if($this->profile->lbox_fullscreen == 'show-icon'): ?>
            <div id="lbox_fullscreen<?php echo $this->uniqueid; ?>" class="lbox_fullscreen lbox_fullscreen_open"></div>
        <?php endif; ?>

        <?php if($this->profile->lbox_allow_rating == 2): ?>
            <?php echo $this->loadTemplate('ratings'); ?>
        <?php endif; ?>

		<?php if($this->profile->lbox_download_image != 'none'): ?>
            <div id="lbox_download_button<?php echo $this->uniqueid; ?>" class="lbox_download_button" >
    	       <a href="#">

    	       </a>
    	   </div>
        <?php endif; ?>
    	
    	<?php if($this->profile->lbox_share_facebook == 1): ?>
			<div id="lbox_facebook_share<?php echo $this->uniqueid; ?>" class="lbox_facebook_share"></div>
        <?php endif; ?>

        <?php if($this->profile->lbox_plus_one == 1): ?>
            <div id="lbox_plus_one_div<?php echo $this->uniqueid; ?>" class="lbox_plus_one_div">
            </div>
        <?php endif; ?>

        <?php if($this->profile->lbox_twitter_button == 1): ?>
            <div id="lbox_twitter_button<?php echo $this->uniqueid; ?>" class="lbox_twitter_button"></div>
        <?php endif; ?>

        <?php if($this->profile->lbox_pinterest_button == 1): ?>
            <div id="lbox_pinterest<?php echo $this->uniqueid; ?>" class="lbox_pinterest"></div>
        <?php endif; ?>

        <?php if($this->profile->lbox_show_image_author == 1): ?>
            <div id="lbox_image_author<?php echo $this->uniqueid; ?>" class="lbox_image_author" >
                <?php echo JText::_( 'JAUTHOR' ) ?>: <span class="lbox_image_author_name" ></span>
            </div>
        <?php endif; ?>

        <?php if($this->profile->lbox_show_image_hits == 1): ?>
            <div id="lbox_image_hits<?php echo $this->uniqueid; ?>" class="lbox_image_hits" >
                <span class="lbox_image_hits_count" ></span> <?php echo JText::_('COM_IGALLERY_VIEWS'); ?>
            </div>
        <?php endif; ?>

        <?php if($this->profile->lbox_show_numbering == 1): ?>
            <div id="lbox_img_numbering<?php echo $this->uniqueid; ?>" class="lbox_img_numbering">
                <span id="lbox_image_number<?php echo $this->uniqueid; ?>"></span>&#47;<?php echo count($this->photoList); ?>
            </div>
        <?php endif; ?>
		
		<?php if($this->profile->lbox_report_image == 1): ?>
			<div id="lbox_report<?php echo $this->uniqueid; ?>" class="lbox_report" >
		       <a href="#" id="lbox_report_link"><?php echo JText::_( 'REPORT_IMAGE' ) ?></a>
		       <form action="index.php?option=com_igallery&amp;task=image.reportImage&id='<?php echo $this->photoList[0]->id; ?>&catid=<?php echo $this->category->id; ?>" method="post" name="lbox_report_form" id="lbox_report_form" style="display: none;">
					<textarea name="report_textarea" id="lbox_report_textarea<?php echo $this->uniqueid; ?>" rows="4" style="width: 100%;"></textarea>
					<input name="Itemid" type="hidden" value="<?php echo JRequest::getInt('Itemid'); ?>" />
					<input type="submit" value="<?php echo JText::_( 'JSUBMIT' ) ?>" />
				</form>
		    </div>
        <?php endif; ?>
		
    </div>

    <?php if($this->profile->lbox_thumb_position == 'right' && $this->profile->lbox_show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_photo_des_position == 'right' && $this->profile->lbox_show_descriptions == 1  && $this->desVars->lboxHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_photo_des_position == 'below' && $this->profile->lbox_show_descriptions == 1  && $this->desVars->lboxHasDescriptions == true): ?>
        <?php echo $this->loadTemplate('descriptions'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_show_tags == 1): ?>
        <?php echo $this->loadTemplate('tags'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_thumb_position == 'below' && $this->profile->lbox_show_thumbs == 1): ?>
        <?php echo $this->loadTemplate('thumbs'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_allow_comments == 2): ?>
        <?php echo $this->loadTemplate('jcomments'); ?>
    <?php endif; ?>

    <?php if($this->profile->lbox_allow_comments == 4 && $this->profile->lbox_comments_position == 'below'): ?>
        <?php echo $this->loadTemplate('fbcomments'); ?>
    <?php endif; ?>

</div>