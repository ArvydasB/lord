<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$thumb_array = $this->mode == 'main' ? $this->thumbFiles : $this->lboxThumbFiles;
$thumbMaxWidth = $this->mode == 'main' ? $this->profile->thumb_width : $this->profile->lbox_thumb_width;
$thumbMaxHeight = $this->mode == 'main' ? $this->profile->thumb_height : $this->profile->lbox_thumb_height;

$largestWidth = 0;
foreach($thumb_array as $thumb)
{
    $largestWidth = $thumb['width'] > $largestWidth ? $thumb['width'] : $largestWidth;
}

?>
<div class="igallery_clear"></div>
<div id="<?php echo $this->mode; ?>_thumb_container<?php echo $this->uniqueid; ?>" class="<?php echo $this->mode; ?>_thumb_container" >

<div id="<?php echo $this->mode; ?>_thumb_grid<?php echo $this->uniqueid; ?>" class="<?php echo $this->mode; ?>_thumb_grid" data-maxthumbwidth="<?php echo $thumbMaxWidth; ?>">

    <?php
    $counter = 0;
    $profileValue = $this->mode == 'main' ? 'show_thumb_info' : 'lbox_show_thumb_info';

    if($this->profile->{$profileValue} == 'tags')
    {
        $db	= JFactory::getDBO();
        $query = 'SELECT id FROM #__igallery WHERE parent = 0 ORDER BY ordering LIMIT 1';
        $db->setQuery($query);
        $topCategory = $db->loadObject();
    }

    while( $counter < count($this->photoList) ): ?>

        <?php
        $row = $this->photoList[$counter];
        $thumbClass = ($counter == 0) ? 'active_thumb' : 'inactive_thumb';

        $lazyStart = $this->profile->thumb_lazyload;
        $src = IG_IMAGE_HTML_RESIZE.$thumb_array[$counter]['folderName'].'/'.$thumb_array[$counter]['fullFileName'];
        $srcAttr = $counter > $lazyStart ? 'data-original="'.$src.'"' : 'src="'.$src.'"';
        $lazyClass = $counter > $lazyStart ? 'ig_grid_lazy' : '';
        $heightCss = $counter > $lazyStart ? 'height: '.($thumb_array[$counter]['height'] + 14).'px' : '';

        if($this->source == 'component' && JRequest::getVar('_escaped_fragment_', null) == null)
        {
            $fileHashNoExt = JFile::stripExt($this->photoList[$counter]->filename);
            $fileHashNoRef = substr($fileHashNoExt, 0, strrpos($fileHashNoExt, '-') );
            $link = str_replace('&', '&amp;', $this->currentUrl.'#!'.$fileHashNoRef);

        }
        else
        {
            $link = '#';
        }
        ?>
        <div data-igid="<?php echo $this->mode; ?>-<?php echo $this->uniqueid; ?>-<?php echo $counter + 1; ?>" class="ig_grid_cell ig_thumb_cell <?php echo $thumbClass; ?>" style="width: <?php echo $thumb_array[$counter]['width'] + 14; ?>px; <?php echo $heightCss; ?>">
            <a href="<?php echo $link; ?>" class="imglink">
                <img class="ig_thumb <?php echo $lazyClass; ?>" <?php echo $srcAttr; ?> title="<?php echo igUtilityHelper::clean($row->alt_text); ?>" alt="<?php echo igUtilityHelper::clean($row->alt_text); ?>" />

                <?php if( !empty($this->photoList[$counter]->embed_code) ): ?>
                    <div class="play-overlay"></div>
                <?php endif; ?>
            </a>
            <?php
            $profileValue = $this->mode == 'main' ? 'show_thumb_info' : 'lbox_show_thumb_info';
            $thumbText = '';

            switch($this->profile->{$profileValue})
            {
                case 'description':
                    $thumbText = $row->description;
                    break;
                case 'alt':
                    $thumbText = $row->alt_text;
                    break;
                case 'tags':
                    if( strlen($row->tags) > 0 )
                    {
                        $tagsArray = explode(',', $row->tags);
                        $thumbText = JText::_('TAGS').': ';
                        for($k=0; $k<count($tagsArray); $k++)
                        {
                            $thumbText .= '<a href="'.JRoute::_('index.php?option=com_igallery&view=category&igid='.$topCategory->id.'&igtype=category&ighidemenu=1&igchild=1&searchAll=1&igtags='.$tagsArray[$k].'&Itemid='.$this->Itemid).'">'.$tagsArray[$k].'</a> ';
                        }
                    }
                    break;
                case 'full':
                    $thumbText = str_replace('_',' ',$row->filename);
                    break;
                case 'no-id':
                    preg_match_all('/-[0-9]+/', $row->filename, $matches);
                    $thumbText = str_replace($matches[0][0], '', $row->filename);
                    $thumbText = str_replace('_',' ',$thumbText);
                    break;
                case 'no-ext':
                    $thumbText = str_replace(array('.jpg','.jpeg','.gif','.png'), '', $row->filename);
                    $thumbText = str_replace('_',' ',$thumbText);
                    break;
                case 'no-id-no-ext':
                    preg_match_all('/-[0-9]+/', $row->filename, $matches);
                    $thumbText = str_replace($matches[0][0], '', $row->filename);
                    $thumbText = str_replace(array('.jpg','.jpeg','.gif','.png'), '', $thumbText);
                    $thumbText = str_replace('_',' ',$thumbText);
                    break;
                default:
                    $thumbText = '';
            }
            ?>

            <?php if(!empty($thumbText)): ?>
                <?php if($this->profile->thumb_grid_type == 'justified'): ?>
                    <div class="caption"><?php echo $thumbText; ?></div>
                <?php else: ?>
                    <span class="<?php echo $this->mode; ?>_thumb_text <?php echo $this->mode; ?>_thumb_text_grid"><?php echo $thumbText; ?></span>
                <?php endif; ?>
            <?php endif; ?>

        </div>

    <?php $counter++; ?>
    <?php endwhile; ?>
</div>

</div>


