<?php
defined('_JEXEC') or die;

jimport( 'joomla.application.component.view');

class igalleryViewcategories extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->user         = JFactory::getUser();
		$this->params = JComponentHelper::getParams('com_igallery');
		$this->moderate = $this->params->get('moderate_cat', 0);
        $this->params = JComponentHelper::getParams('com_igallery');

		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$document = JFactory::getDocument();

        if(IG_J30)
        {
            JHtml::_('jquery.framework');
        }
        else
        {
            if($this->params->get('gallery_include_jquery', 0) == 1)
            {
                $document->addScript(JURI::root(true).'/media/com_igallery/plupload/js/jquery.js');
            }
        }

		$document->addScript(JURI::root(true).'/media/com_igallery/js/admin.js');
		$document->addStyleSheet(JURI::root(true).'/media/com_igallery/css/admin.css');

		parent::display($tpl);
	}
}