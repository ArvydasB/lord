jQuery(window).load(function()
{
    //check all
    if(jQuery('#ig_check_all').length)
    {
        jQuery('#ig_check_all').on('click', function()
        {
            var checkBoxes =jQuery('#ig_admin_table').find('input[class=ig_checkbox]');
            if(jQuery('#ig_check_all').is(':checked'))
            {
                checkBoxes.each(function(index,el)
                {
                    jQuery(el).prop('checked',true);
                });
            }
            else
            {
                checkBoxes.each(function(index,el)
                {
                    jQuery(el).prop('checked',false);
                });
            }
        });
    }

    //category list
    if(jQuery('#ig_toolbar_cat_pub').length)
    {
        jQuery('#ig_toolbar_cat_pub').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_toolbar_cat_pub').attr('href');

            var fullUrl = igGetUrl(baseUrl, false);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    if(jQuery('#ig_toolbar_cat_unpub').length)
    {
        jQuery('#ig_toolbar_cat_unpub').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_toolbar_cat_unpub').attr('href');

            var fullUrl = igGetUrl(baseUrl, false);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    if(jQuery('#ig_saveorder').length)
    {
        jQuery('#ig_saveorder').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_saveorder').attr('href');

            var fullUrl = igGetUrl(baseUrl, true);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    //category item
    if(jQuery('#ig_toolbar_cat_apply').length)
    {
        jQuery('#ig_toolbar_cat_apply').on('click', function(event)
        {
            event.preventDefault();
            jQuery('#ig_task').val('icategory.apply');
            jQuery('#ig_new_cat_form').submit();
        });
    }

    if(jQuery('#ig_toolbar_cat_save').length)
    {
        jQuery('#ig_toolbar_cat_save').on('click', function(event)
        {
            event.preventDefault();
            jQuery('#ig_task').val('icategory.save');
            jQuery('#ig_new_cat_form').submit();
        });
    }

    //images list
    if(jQuery('#ig_toolbar_image_pub').length)
    {
        jQuery('#ig_toolbar_image_pub').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_toolbar_image_pub').attr('href');

            var fullUrl = igGetUrl(baseUrl, false);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    if(jQuery('#ig_toolbar_image_unpub').length)
    {
        jQuery('#ig_toolbar_image_unpub').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_toolbar_image_unpub').attr('href');

            var fullUrl = igGetUrl(baseUrl, false);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    if(jQuery('#ig_toolbar_image_delete').length)
    {
        jQuery('#ig_toolbar_image_delete').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_toolbar_image_delete').attr('href');

            var fullUrl = igGetUrl(baseUrl, false);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    if(jQuery('#ig_saveorder').length)
    {
        jQuery('#ig_saveorder').on('click', function(event)
        {
            event.preventDefault();
            var baseUrl = jQuery('#ig_saveorder').attr('href');

            var fullUrl = igGetUrl(baseUrl, true);
            if(fullUrl == false){return;}
            window.location = fullUrl;
        });
    }

    //image item
    if(jQuery('#ig_toolbar_image_apply').length)
    {
        jQuery('#ig_toolbar_image_apply').on('click', function(event)
        {
            event.preventDefault();
            jQuery('#ig_task').val('image.apply');
            jQuery('#ig_image_form').submit();
        });
    }

    if(jQuery('#ig_toolbar_image_save').length)
    {
        jQuery('#ig_toolbar_image_save').on('click', function(event)
        {
            event.preventDefault();
            jQuery('#ig_task').val('image.save');
            jQuery('#ig_image_form').submit();
        });
    }

    if(jQuery('#ig_toolbar_image_savenext').length)
    {
        jQuery('#ig_toolbar_image_savenext').on('click', function(event)
        {
            event.preventDefault();
            jQuery('#ig_task').val('image.save_and_next');
            jQuery('#ig_image_form').submit();
        });
    }


});

function igGetUrl(baseUrl, addOrder)
{
    var ids = new Array();
    var checkBoxes = jQuery('#ig_admin_table').find('input[class=ig_checkbox]');

    checkBoxes.each(function(index,el)
    {
       if(jQuery(el).is(':checked') || addOrder == true)
       {
            var id = parseInt( jQuery(el).attr('name').replace('ig_check_','') );
            ids.push(id);
       }
    });

    if(addOrder)
    {
        var orderNumbers = new Array();
        var orderBoxes = jQuery('#ig_admin_table').find('input[class=ig_order_box]');

        orderBoxes.each(function(index,el)
        {
            var order = parseInt( jQuery(el).val() );
            orderNumbers.push(order);
        });
    }

    if(ids.length > 0)
    {
        var cidString = '';
        for(var i=0;i<ids.length;i++)
        {
            cidString = i==0 ? cidString + 'cid[]=' + ids[i] : cidString + '&cid[]=' + ids[i];
        }
        var fullUrl = baseUrl.indexOf('?') == -1 ? baseUrl + '?' + cidString : baseUrl + '&' + cidString;

        if(addOrder)
        {
            var orderString = '';
            for(var i=0;i<orderNumbers.length;i++)
            {
                orderString = orderString + '&order[]=' + orderNumbers[i];
            }

            fullUrl = fullUrl + orderString;
        }
        return fullUrl;
    }
    else
    {
        alert('Please select a category');
        return false;
    }
}