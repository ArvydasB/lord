<?php
/**
 * copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

defined('_JEXEC') or die;
if ($this->error->getCode() == '404') {
	http_response_code("404");
	echo file_get_contents(JURI::root().'404');
	exit;
}

?>
