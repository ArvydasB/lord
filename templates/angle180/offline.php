<?php
/**
 * @copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * @license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

defined('_JEXEC') or die;
if (!isset($this->error)) {
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}

jimport('joomla.user.helper');
$user_id = JUserHelper::getUserId('admin');
$uinfo = JFactory::getUser($user_id);

$mail = $uinfo->email;


$err_code = $this->error->getCode();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo JURI::base(). 'templates/'.$this->template.'/css/bootstrap.min.css'; ?>">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			var wh = jQuery(window).height();
			var dh = jQuery(".col-md-6").height();
			jQuery(".col-md-6").css("margin-top",((wh/2)-(dh-2)));
			console.log("<?php echo $err_code; ?>");
		})
	</script>
	<style>
		.extra { font-size:150px; font-weight:bold; line-height:85px; padding-bottom:20px; margin-bottom:20px; color:#fff; text-align:center;}
		.col-md-6 { margin:0 auto; float:none!important;}
		.colored {
			background:#ffa11b;
			color:#fff;
		}
		.ttt { margin-top:0px!important;}
		#smart-mobile-menu {
			display:none;
		}
		#smart-desktop-menu, #smart-desktop-menu li, #smart-desktop-menu li a {text-align:center; background:transparent!important; border:0px solid transparent!important; color:#333!important; list-style-type:none!important; text-align:center; }
		#smart-desktop-menu ul.menu { max-width:50%; margin:0 auto;}
		#smart-desktop-menu ul.menu li { width:100%; }	
	</style>

</head>
<body>
	<div class="container-fluid" style="margin:0px; padding:0px;">
		<div class="row">
			<div class="colored" style="width:100%; padding-bottom:20px; height:45%; display:block; overflow:hidden;">
				<div class="col-xs-6 col-md-6 col-sm-6 col-ld-6">
						<span style="font-size:30px;">Temporarily Down for</span><br />
						<span style="font-size:58px; font-weight:bold;">Maintenance</span>
				</div>
			</div>
			<div class="row" style="width:100%; margin-top:20px; height:auto; overflow:hidden;">
					<div class="col-xs-6 col-md-6 col-sm-6 col-ld-6 ttt" style="margin-top:0px;">
						<p style="color:#333;">
							<span style="font-size:20px;">We are performing scheduled maintenance. <br />We should be back online shortly.</span>
						</p> 
					</div>
			</div>
		</div>
	</div>

</body>
</html>
