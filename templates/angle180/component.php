<?php
/**
 * @copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * @license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

// No direct access.
defined('_JEXEC') or die;

$color = $this->params->get('templatecolor');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<link href="./templates/angle180/css/template.css" rel="stylesheet" type="text/css">
	<script src="./templates/angle180/js/jquery.min.js" type="text/javascript"></script>
	<script src="./templates/angle180/js/bootstrap.js" type="text/javascript"></script>
	<script src="./templates/angle180/js/template.js" defer="defer" async="async"></script>
<head>
</head>
<body class="comp">
	<div id="">
	<div id="">
		<jdoc:include type="message" />
		<jdoc:include type="component" />
		</div>
	</div>
</body>
</html>
