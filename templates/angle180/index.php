<?php
/**
* @copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
* @license		Commercial Template. Do not use without written permission on Angle180 LLC.
*/


error_reporting(E_ALL);
ini_set("display_errors",1);

// No direct access.
defined('_JEXEC') or die('Restricted access');

if (trim(JFactory::getApplication()->get("error_reporting")) == "development") {
	error_reporting(E_ALL);
	ini_set("display_errors",1);
}

include getcwd() . "/templates/".$this->template."/core/config.php";
include getcwd() . "/templates/".$this->template."/core/modrender.php";

$app = JFactory::getApplication();

$sitename = $app->getCfg('sitename');
$desc = $doc->getMetaData("description");

$menu = $app->getMenu();
$active_menu = $menu->getActive();

?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<?php if( $this->params->get("schema_url") ): ?>
		<?php if($menu->getActive() == $menu->getDefault() ) : ?>
			<?php
			$schema_url = sprintf("%s:\/\/%s\/", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
			$schema_name = $doc->getTitle();
			$schema_alternate_name = $this->params->get('schema_alternate_name');
			$schema_alternate_url = $this->params->get('schema_alternate_url');
			?>
			<script type='application/ld+json'>{"@context":"http:\/\/schema.org","@type":"WebSite","url":"<?php echo $schema_url ?>","name":"<?php echo $schema_name; ?>","alternateName":"<?php echo $schema_alternate_name; ?>","potentialAction":{"@type":"SearchAction","target":"http:\/\/<?php echo $schema_alternate_url; ?>\/?s={search_term}","query-input":"required name=search_term"}}</script>
		<?php endif; ?>
	<?php endif; ?>

	<jdoc:include type="head" />

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="favicon/manifest.json">
	<link rel="mask-icon" href="favicon/safari-pinned-tab.svg" color="#689ee0">
	<meta name="theme-color" content="#fdac1e">

	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<meta property="og:site_name" content="<?php echo $doc->getTitle() ; //$sitename; ?>"/>
	<meta property="og:title" content="<?php echo JFactory::getDocument()->getTitle(); ?>" />
	<meta property="og:description" content="<?php echo $desc; ?>" />	
</head>

<body class="<?php echo (($menu->getActive() == $menu->getDefault()) ? 'default-page' : 'inner-page ') . " " . $active_menu->params["pageclass_sfx"]; ?>">

	<jdoc:include type="modules" name="mobile-menu" style="xhtml"/>

	<?php
/*
* HEADER:
* This is header section with 3
* module positions.
*/
?>
<div id="header">
	<?php if($this->params->get("header1-1fluid")) {
		echo "<div class='container'><div class='row'>";
	} ?>
	<?php renderModule("header1", $this); ?>


	<?php if ((!$this->params->get("header1-1fluid")) && ($this->params->get("header1-2fluid"))) {
		echo "<div class='container'><div class='row'>";
	} else if (($this->params->get("header1-1fluid")) && (!$this->params->get("header1-2fluid"))) {
		echo "</div></div>";
	} ?>
	<?php renderModule("header2", $this); ?>


	<?php if ((!$this->params->get("header1-2fluid")) && ($this->params->get("header1-3fluid"))) {
		echo "<div class='container'><div class='row'>";
	} else if (($this->params->get("header1-2fluid")) && (!$this->params->get("header1-3fluid"))) {
		echo "</div></div>";
	} ?>
	<?php renderModule("header3", $this); ?>


	<?php if ((!$this->params->get("header1-3fluid")) && ($this->params->get("header1-4fluid"))) {
		echo "<div class='container'><div class='row'>";
	} else if (($this->params->get("header1-3fluid")) && (!$this->params->get("header1-4fluid"))) {
		echo "</div></div>";
	} ?>
	<?php renderModule("header4", $this); ?>
	<?php if ($this->params->get("header1-4fluid")) {
		echo "</div></div>";
	} ?>
	<!--</div>-->
</div>



<?php
/*
* SEPARATOR1:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator1")) { ?>
<div class="separator separator1">
	<?php renderModule("separator1", $this); ?>
</div>
<?php } ?>







<?php
/*
* SECTION1:
* This is section1 and it has 2 module positions
*/
?>
<?php if (($this->countModules("section1-1") > 0) || ($this->countModules("section1-2") > 0)): ?>

	<div id="section1">
		<?php if ($this->params->get("section1-1fluid")) {
			echo "<div class='container'><div class='row'>";
		}
		renderModule("section1-1", $this);
		if ((!$this->params->get("section1-1fluid")) && ($this->params->get("section1-2fluid"))) {
			echo "<div class='container'><div class='row'>";
		} else if (($this->params->get("section1-1fluid")) && (!$this->params->get("section1-2fluid"))) {
			echo "</div></div>";
		}
		renderModule("section1-2", $this);
		if ($this->params->get("section1-2fluid")) {
			echo "</div></div>";
		} ?>
		<!--</div>-->
	</div>

<?php endif; ?>




<?php
/*
* SEPARATOR2:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator2")) { ?>
<div class="separator separator2">
	<?php renderModule("separator2", $this); ?>
</div>
<?php } ?>







<?php
/*
* SECTION2:
* This is section1 and it has 2 module positions
*/
?>
<?php if (($this->countModules("section2-1") > 0) || ($this->countModules("section2-2") > 0)): ?>

	<div id="section2">
		<?php if ($this->params->get("section2-1fluid")) {
			echo "<div class='container'><div class='row'>";
		}
		renderModule("section2-1", $this);
		if ((!$this->params->get("section2-1fluid")) && ($this->params->get("section2-2fluid"))) {
			echo "<div class='container'><div class='row'>";
		} else if (($this->params->get("section2-1fluid")) && (!$this->params->get("section2-2fluid"))) {
			echo "</div></div>";
		}
		renderModule("section2-2", $this);
		if ($this->params->get("section2-2fluid")) {
			echo "</div></div>";
		} ?>
		<!--</div>-->
	</div>

<?php endif; ?>






<?php
/*
* SEPARATOR3:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator3")) { ?>
<div class="separator separator3">
	<?php renderModule("separator3", $this); ?>
</div>
<?php } ?>






<?php
/*
* MAINCONTENT
* ---
* This is content with sidebars block
*/

if (!$this->countModules("sidebar3")){
	?>
		<div class="mainContent">
			<?php /* No Sidebars */ ?>
			<?php if (!$this->countModules("sidebar1") && !$this->countModules("sidebar2")) { ?>

					<div class="<?php echo $this->params->get('content_class'); ?>">
						<?php if ($this->params->get("hide_content") != JRequest::getVar("Itemid")): ?>
							<jdoc:include type="component" />
						<?php endif; ?>
					</div>

			<?php } ?>


			<?php /* When just sidebar1 is active */ ?>
			<?php if ($this->countModules("sidebar1") && !$this->countModules("sidebar2")) { ?>
			<div class="container">
				<div class="row">
					<div class=" <?php echo $this->params->get('content_class'); ?> fright">
						<?php if ($this->params->get("hide_content") != JRequest::getVar("Itemid")): ?>
							<jdoc:include type="component" />
						<?php endif; ?>
					</div>
					<?php renderModule("sidebar1", $this); ?>
				</div>
			</div>
			<?php } ?>


			<?php /* When just sidebar2 is active */ ?>
			<?php if (!$this->countModules("sidebar1") && $this->countModules("sidebar2")) { ?>
			<div class="container">
				<div class="row">
					<div class=" <?php echo $this->params->get('content_class'); ?> fleft">
						<?php if ($this->params->get("hide_content") != JRequest::getVar("Itemid")): ?>
							<jdoc:include type="component" />
						<?php endif; ?>
					</div>
					<?php renderModule("sidebar2", $this); ?>
				</div>
			</div>
			<?php } ?>


			<?php /* When both sidebars is active */ ?>
			<?php if ($this->countModules("sidebar1") && $this->countModules("sidebar2")) { ?>
			<div class="container">
				<div class="row">
					<div class=" <?php echo $this->params->get('content_class'); ?>">
						<?php if ($this->params->get("hide_content") != JRequest::getVar("Itemid")): ?>
							<jdoc:include type="component" />
						<?php endif; ?>
					</div>
					<?php renderModule("sidebar1", $this); ?>
					<?php renderModule("sidebar2", $this); ?>
				</div>
			</div>
			<?php } ?>
		</div>
	<?php
} ?>






<?php
/*
* SEPARATOR4:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator4")) { ?>
<div class="separator separator4">
	<?php renderModule("separator4", $this); ?>
</div>
<?php } ?>







<?php
/*
* SECTION3:
* This is section3 and it has 2 module positions
*/
?>
<?php if (($this->countModules("section3-1") > 0) || ($this->countModules("section3-2") > 0)): ?>

	<div id="section3">
		<?php if ($this->params->get("section3-1fluid")) {
			echo "<div class='container'><div class='row'>";
		}
		renderModule("section3-1", $this);
		if ((!$this->params->get("section3-1fluid")) && ($this->params->get("section3-2fluid"))) {
			echo "<div class='container'><div class='row'>";
		} else if (($this->params->get("section3-1fluid")) && (!$this->params->get("section3-2fluid"))) {
			echo "</div></div>";
		}
		renderModule("section3-2", $this);
		if ($this->params->get("section3-2fluid")) {
			echo "</div></div>";
		} ?>
		<!--</div>-->
	</div>

<?php endif; ?>










<?php
/*
* SEPARATOR5:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator5")) { ?>
<div class="separator separator5">
	<?php renderModule("separator5", $this); ?>
</div>
<?php } ?>






<?php
/*
* SECTION4:
* This is section4 and it has 2 module positions
*/
?>
<?php if (($this->countModules("section4-1") > 0) || ($this->countModules("section4-2") > 0)): ?>

	<div id="section4">
		<?php if ($this->params->get("section4-1fluid")) {
			echo "<div class='container'><div class='row'>";
		}
		renderModule("section4-1", $this);
		if ((!$this->params->get("section4-1fluid")) && ($this->params->get("section4-2fluid"))) {
			echo "<div class='container'><div class='row'>";
		} else if (($this->params->get("section4-1fluid")) && (!$this->params->get("section4-2fluid"))) {
			echo "</div></div>";
		}
		renderModule("section4-2", $this);
		if ($this->params->get("section4-2fluid")) {
			echo "</div></div>";
		} ?>
		<!--</div>-->
	</div>

<?php endif; ?>



<?php if (($this->countModules("section7-1") > 0) || ($this->countModules("section7-2") > 0)): ?>

	<div id="section7" class="section">
		<?php 
		renderModule("section7-1", $this);
		
		renderModule("section7-2", $this);
		?>
		
	</div>

<?php endif; ?>




<?php
/*
* SEPARATOR6:
* This is module position which is always
* fluid and usable just like helper
*/
?>
<?php if ($this->countModules("separator6")) { ?>
<div class="separator separator6">
	<?php renderModule("separator6", $this); ?>
</div>
<?php } ?>







<?php
/*
* FOOTER:
* This is section4 and it has 2 module positions
*/
?>
<?php if (($this->countModules("footer1") > 0) || ($this->countModules("footer2") > 0)): ?>

	<div id="footer">
		<?php if ($this->params->get("footer1fluid")) {
			echo "<div class='container'><div class='row'>";
		}
		renderModule("footer1", $this);
		if ((!$this->params->get("footer1fluid")) && ($this->params->get("footer2fluid"))) {
			echo "<div class='container'><div class='row'>";
		} else if (($this->params->get("footer1fluid")) && (!$this->params->get("footer2fluid"))) {
			echo "</div></div>";
		}
		renderModule("footer2", $this);
		if ($this->params->get("footer2fluid")) {
			echo "</div></div>";
		} ?>
		<!--</div>-->
	</div>

<?php endif; ?>

<?php if (($this->countModules("modal1") > 0)): ?>

	<div id="modal">
		<?php 
		renderModule("modal1", $this);	
		?>
	</div>

<?php endif; ?>



<?php /* Bottom JS initilaization from backend */ ?>
<script>
	<?php echo $this->params->get('apiintegration'); ?>
</script>

<script>

</script>
<?php /* Script for com_processWork */ ?>
<script>
	jQuery(document).ready(function() {
		jQuery("form").append("<input type='hidden' name='<?php echo JSession::getFormToken(); ?>' value='1' />");
	})
</script>


</body>
</html>
