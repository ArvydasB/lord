<?php
defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.form.formfield');
class JFormFieldfonts extends JFormField {
        protected $type = 'fonts';
 
        public function getLabel() {
        $label = '';
        $replace = '';

        $text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];

        $class = !empty($this->description) ? 'hasTip' : '';
        $class = $this->required == true ? $class.' required' : $class;

        $label .= '<label id="'.$this->id.'-lbl" for="'.$this->id.'" class="'.$class.'"';
 
        if (!empty($this->description)) {
                $label .= ' title="'.htmlspecialchars(trim(JText::_($text), ':').'::' .
                                JText::_($this->description), ENT_COMPAT, 'UTF-8').'"';
        }

        $label .= '>'.$replace.JText::_($text).'</label>';
 
        return $label; 
        }


        public function fontai() {
                $contents = file_get_contents(JURI::root() . "?tmpl=fonts");
                $fnt = json_decode($contents); 
        	return $fnt; //json_decode();
        }


        public function getInput() {
                
        	$font_arr = $this->fontai();
                $html = "<select id='".$this->id."' name='".$this->name."'>";
                foreach($font_arr as $fnt) {
                	if ($fnt->id == $this->value) {
                		$html .= "<option value='".$fnt->id."' selected='selected'>".$fnt->name."</option>";
                	} else {
                		$html .= "<option value='".$fnt->id."'>".$fnt->name."</option>";
                	}
                }
                $html .= "</select>";
                return $html;
        }
}