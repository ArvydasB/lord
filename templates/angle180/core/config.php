<?php
/**
 * @copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * @license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

// No direct access.
defined('_JEXEC') or die;
$doc = JFactory::getDocument();

//Security checks for hidden Front components
if ($this->params->get("sec_com_users") && JRequest::getVar("option") == "com_users") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_weblinks") && JRequest::getVar("option") == "com_weblinks") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_newsfeeds") && JRequest::getVar("option") == "com_newsfeeds") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_banners") && JRequest::getVar("option") == "com_banners") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_search") && JRequest::getVar("option") == "com_search") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_finder") && JRequest::getVar("option") == "com_finder") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_content") && JRequest::getVar("option") == "com_content") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}
if ($this->params->get("sec_com_k2") && JRequest::getVar("option") == "com_k2") {
	$app = JFactory::getApplication();
	$link = "index.php";
	$msg = "Sorry, the page you are looking for doesnt exists!";
	$app->redirect($link, $msg, $msgType='error');
}


$removeDefaults = array(
    "/media/jui/js/jquery.js",
    "/media/jui/js/jquery.min.js",
    "/media/jui/js/jquery-noconflict.js",
    "/media/jui/js/jquery-migrate.min.js"
);

foreach($doc->_scripts as $key => $script) {
    if(in_array($key, $removeDefaults)) {
        unset($doc->_scripts[$key]);
    }
}

//CSS RENDERER

$doc->addStyleSheet(JURI::base() . "templates/".$this->template."/css/template.css");


//JS RENDERER
$doc->addScript(JURI::base() . "templates/".$this->template."/js/jquery.min.js");
$doc->addScript(JURI::base() . "templates/".$this->template."/js/bootstrap.min.js");
$doc->addScript(JURI::base() . "templates/".$this->template."/js/bootstrap-select.min.js");
$doc->addScript(JURI::base() . "templates/".$this->template."/js/template.js", NULL, true, true);
