<?php
/**
 * @copyright	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * @license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

// No direct access.
defined('_JEXEC') or die;



function renderModule($module, $tmpl) {
//HEADER
	$param['header1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['header2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['header3'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['header4'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
//SECTION1    
    $param['section1-1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['section1-2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
//SECTION2    
    $param['section2-1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['section2-2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
//SIDEBARS    
    $param['sidebar1'] = "fleft ccol-lg-3 col-md-3 col-sm-3 col-xs-12";
	$param['sidebar2'] = "fright col-lg-3 col-md-3 col-sm-3 col-xs-12";
//SECTION3    
    $param['section3-1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['section3-2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12"; 
//SECTION4    
    $param['section4-1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['section4-2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";    
//FOOTER    
    $param['footer1'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";
	$param['footer2'] = "col-lg-6 col-md-6 col-sm-6 col-xs-12";    
    
	if ($tmpl->countModules($module)) {
		$mobile = $tmpl->params->get($module . "_onmobile");
		$tablet = $tmpl->params->get($module . "_ontablet");
		$desktop = $tmpl->params->get($module . "_ondesktop");

		$resp = "";
		if ($mobile == "0") {
			$resp .= " hidden-xs ";
			$param[$module] .= " hidden-xs ";
		}
		if ($tablet == "0") {
			$resp .= " hidden-sm ";
			$param[$module] .= " hidden-sm ";
		}
		if ($desktop == "0") {
			$resp .= " hidden-lg hidden-md ";
			$param[$module] .= " hidden-md hidden-lg ";
		}

// New stuff for displaying date in copyright
		$mod_plugin = JModuleHelper::getModules($module);
		$inmod = 0;

		foreach ($mod_plugin as $mdl)
		{
			if (preg_match("/\#CYear\#/",$mod_plugin[$inmod]->content))
			{
				$tmp = preg_replace("/\#CYear\#/", date("Y"), $mod_plugin[$inmod]->content);

				$moduleToUpdate = JModuleHelper::getModules($module);
				$moduleToUpdate[$inmod]->content = $tmp;
			}
			$inmod = $inmod+1;
		}

		if ($tmpl->params->get($module . "_class")) { ?>

			<div id="<?php echo $module; ?>" class="<?php echo $resp . $tmpl->params->get($module . '_class'); ?>">
		<?php } else if (array_key_exists($module, $param)) { ?>
			<div id="<?php echo $module; ?>" class="<?php echo $resp . $param[$module]; ?>">
		<?php } else { ?>
			<div id="<?php echo $module; ?>" class="<?php echo $resp; ?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<?php } ?>
		
				<jdoc:include type="modules" name="<?php echo $module; ?>" style="xhtml"/>
			</div><?php
	}
}
?>
