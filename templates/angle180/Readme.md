# Angle180 Joomla main Template TOC:

*   [To Do List](#markdown-header-todo)
*   [Menu](#markdown-header-menu)
*   [Css](#markdown-header-css)
    *   [sass](#markdown-header-sass)
    *   [bourbon](#markdown-header-bourbon)
*   [JavaScript](#markdown-header-js)



 * @angle180inc 	Copyright (C) 2014 Angle180 LLC. All rights reserved.
 * @angle180inc		Commercial Template. Do not use without written permission on Angle180 LLC.





# Todo

1. Moduliu, komponentu... perdengimu perziura, kodo valymas ir inprovement'as!
2. Modalinio lango integracija, valdmas ir etc.
3. Nauju font iconeliu "Glyph" integracija, bet reikia gaut is dizainerio filus.
# Menu


[Martyno](https://bitbucket.org/Martinis93/) meno kurinys. Jis pagal nutilejima yra template dalis todel pakitimu daryma perimsiu asmeniskai 
kaip viso template tobulinima. 
Naudojimas paprastas! Tereikia sukurti 2 meniu modulius. 

1. Mobile meniu modulis su parinkta "**Alternative Layout**" opcija "**MartinMenu**" 
2. Desktop ir Tablet galima pasirinkti koki norim arba palikti default (bet kokiu atveju reikes susikurti stiliu)

Norint jog tik sukurus modulius veiktu responsive meniu. Prie parametru "Menu Class Suffix" darasome:

1. Desktop menu - " desktopmenu"
2. Mobile menu - " mobilemenu"

**Zinoma be kabuciu! Taip pat senieji meniu perdengimai neistrinti ir yra savo vietose jei nauji netinka!**

Jei ant tablet reikia atvaizduoti mobile menu tokiu atveju reikia [pakeisti menu.scss sass faila](/sass/menu.scss) ir perkompiliuoti stilius ([Kaip tai padaryti?](#markdown-header-sass))




# Css

Cia pateikiu pateikiama informacija susijusi su template stiliais kurie labai smarkiai pakito.
Beveik visi css failai pakeisti ir siuo metu butina skaityti sia dokumentacija!

# Sass

Nenustebk [template css](css) kataloge liko tik 1 failas, realiai svetainei reikalingas tik jis 
nes is [scss katalogo](scss) bus sugeneruojamas vinetas **template.css**. Zinoma neskaitant pluginu kuriuos
naudosi, jie iterpiami kaip paprasti css arba galima ikompiliuoti i sass.(Asmeniskai as ikompiliuociau i template.css)
Kompiliacija vyksta taip:

```
sass --watch sass/template.scss:css/template.css --style compressed
```

Kur /sass/template.scss faile yra includinami visi kiti css'ai is scss katalogo!

# Bourbon

Kai jau zinosi kas yra sass ir busi bent minimaliai susidraugaves su juo gali paskaityti apie [bourbon'a kuris integruotas i template](http://bourbon.io/)

# Js

Template Javascript dalis labai smarkiai pakeista. Frontend dalyje isimti beveik visi standartiniai Joomlos JS'ai.
Taip pat isimta standartine jQuery versija kuri pakeista i 2.2.4. 
Jog nepradetume vel siukslinti template.js faile perdariau jo struktura, pagal kuria dabar yra 3 event'ai:

1. [document.ready](https://learn.jquery.com/using-jquery-core/document-ready/)
2. [window.load](https://api.jquery.com/load-event/)
3. [window.resize](https://api.jquery.com/resize/)

Siuose eventuose bus iskvieciamos reikiamos funkcijos kurios bus aprasytos failo apacioje, o ne padrikas kodas kuris ilgainiui uzsimirsta
ir tampa siuksle!
