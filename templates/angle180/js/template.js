/**
* @author  ( Angle180 LLC  )
* @copyright Copyright (C) 2014 Angle180 LLC. All rights reserved.
* @license Commercial Template. Do not use without written permission on Angle180 LLC.
*/

var preloader = new Array(
	)
jQuery(function () {
  jQuery('[data-toggle="popover"]').popover();
});
jQuery(function () {
  jQuery('[data-toggle="tooltip"]').tooltip();
});


var allCategoryItems;

jQuery(window).load(function(){
	allCategoryItems = document.querySelectorAll(".category-item");
});



jQuery('.selectpicker').on('changed.bs.select', function (e) {
	filterCategoryItem();
});


function filterCategoryItem() {
	var year = jQuery(".selectpicker.year").selectpicker('val');
	var letter = jQuery(".selectpicker.letter").selectpicker('val');
	var father = jQuery(".selectpicker.father").selectpicker('val');
	var mother = jQuery(".selectpicker.mother").selectpicker('val');

	for ( var i = 0; i < allCategoryItems.length; i++) {
		allCategoryItems[i].dataset.showy = "hide";
		allCategoryItems[i].dataset.showl = "hide";
		allCategoryItems[i].dataset.showf = "hide";
		allCategoryItems[i].dataset.showm = "hide";
	}

	if (year != null){
		for (var a = 0; a < allCategoryItems.length; a++){
			if (allCategoryItems[a].dataset.year.indexOf(year) !== -1){
				allCategoryItems[a].dataset.showy = 'show';
			}
		}
	} else {
		for (var a = 0; a < allCategoryItems.length; a++){
			allCategoryItems[a].dataset.showy = 'show';
		}
	}

	if (letter != null){
		for (var a = 0; a < allCategoryItems.length; a++){
			if (allCategoryItems[a].dataset.letter.indexOf(letter) !== -1){
				allCategoryItems[a].dataset.showl = 'show';
			}
		}
	} else {
		for (var a = 0; a < allCategoryItems.length; a++){
			allCategoryItems[a].dataset.showl = 'show';
		}
	}

	if (father != null){
		for (var a = 0; a < allCategoryItems.length; a++){
			if (allCategoryItems[a].dataset.father.indexOf(father) !== -1){
				allCategoryItems[a].dataset.showf = 'show';
			}
		}
	} else {
		for (var a = 0; a < allCategoryItems.length; a++){
			allCategoryItems[a].dataset.showf = 'show';
		}
	}

	if (mother != null){
		for (var a = 0; a < allCategoryItems.length; a++){
			if (allCategoryItems[a].dataset.mother.indexOf(mother) !== -1){
				allCategoryItems[a].dataset.showm = 'show';
			}
		}
	} else {
		for (var a = 0; a < allCategoryItems.length; a++){
			allCategoryItems[a].dataset.showm = 'show';
		}
	}


	for (var i = 0; i < allCategoryItems.length; i++){
		var elem = jQuery(allCategoryItems[i]);
		var catItem = allCategoryItems[i];
		if (catItem.dataset.showy == 'show' && catItem.dataset.showl == 'show' && catItem.dataset.showf == 'show' && catItem.dataset.showm == 'show') {
			if (!elem.is(":visible")) {
				elem.fadeIn();
			}
		} else {
			if (elem.is(":visible")) {
				elem.fadeOut();
			}
		}
	}
}



// Document Ready state
// preload images
// do stuff while page is not renderd
document.onready = function() {
//Preload images
if (preloader.length > 0) {
	preload(preloader)
}
jQuery('.carousel').carousel({
	interval: 5000
});
}

// Window Read state
// Page is rendered now can display
// some elements!
window.onload = function() {








	if(jQuery(window).width() > 1025 ){
		setTimeout(function () {
			jQuery('body').fadeIn(100);
		}, 100);
	}

	if( jQuery(window).width() <= 1024 ){
		setTimeout(function () {
			jQuery('body').hide().fadeIn(100);
		}, 100);
	}

//-- Back to top start --//
var scrollTopBtn = jQuery('.scroll-to-top');
var offset = 300;
var scrollTopDuration = 700;
var lastScrollTop = 0;
jQuery(window).scroll(function() {
	var st = jQuery(this).scrollTop();
	if (jQuery(this).scrollTop() > offset) {
		if (st >= lastScrollTop) {
			jQuery('.scroll-to-top').removeClass('is-active');
		} else {
			jQuery('.scroll-to-top').addClass('is-active');
		}
		lastScrollTop = st;
	} else {
		jQuery('.scroll-to-top').removeClass('is-active');
		lastScrollTop = 0;
	}
});
jQuery(document).on('click', '.scroll-to-top', function(e) {
	e.preventDefault();
	jQuery('body,html').animate({
		scrollTop: 0
	}, scrollTopDuration);
});
//-- Back to top end --//

//-- Mobile menu body scroll fix start--//
jQuery("input#toggle").on("change", function() {
	if (jQuery('#toggle:checked').length > 0){
		jQuery("body").addClass("mobile-activated");
		jQuery("html").addClass("mobile-activated");
	} else {
		jQuery("body").removeClass("mobile-activated");
		jQuery("html").removeClass("mobile-activated");
	}
});
//-- Mobile menu body scroll fix end--//
}

//Window Resize action
window.onresize = function() {

}


/*
* Functions:
* - preload(preloader) => Preload images in array of preload variable
*/

function preload(images) {
	var img = new Array();
	for (var x=0; x<=(images.length-1); x++) {
		img[x] = new Image()
		img[x].src=images[x]
	}
}
