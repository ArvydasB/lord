<?php defined('_JEXEC') or die; ?>
<div class="hey-mobile-nav">

	<div class="hey-mobile-nav-header">
		<button class="hey-mobile-nav-toggle"><span class="bar-1">-</span><span class="bar-2">-</span><span class="bar-3">-</span></button>
		<div class="hey-mobile-nav-page-title">
		<?php
			$active = JFactory::getApplication()->getMenu()->getActive();
			$title = $active->title;
			if(strlen($title) > 50)
				if (preg_match('/^.{1,52}\b/s', $title, $match)) {
				$title = $match[0].'&hellip;';
				}
			echo $title;

			echo JHTML::_('content.prepare', '{loadposition mobile-menu-header}');
		?>
		</div>
	</div>

	<div class="hey-mobile-nav-container">
		<ul class="hey-mobile-nav-menu<?php echo $class_sfx;?>"<?php
			$tag = '';

			if ($params->get('tag_id') != null)
			{
				$tag = $params->get('tag_id') . '';
				echo ' id="' . $tag . '"';
			}
		?>>

		<?php
			$search_position = JHTML::_('content.prepare', '{loadposition mobile-menu-search}');
			if($search_position != "") :
		?>
		<li id="hey-mobile-nav-search">
			<div>
				<?php echo $search_position;?>
			</div>
		</li>
		<?php endif; ?>

		<?php	

		foreach ($list as $i => &$item)
		{
			$class = 'item-' . $item->id;

			if (($item->id == $active_id) OR ($item->type == 'alias' AND $item->params->get('aliasoptions') == $active_id))
			{
				$class .= ' current';
			}

			if (in_array($item->id, $path))
			{
				$class .= ' active';
			}
			elseif ($item->type == 'alias')
			{
				$aliasToId = $item->params->get('aliasoptions');

				if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
				{
					$class .= ' active';
				}
				elseif (in_array($aliasToId, $path))
				{
					$class .= ' alias-parent-active';
				}
			}

			if ($item->type == 'separator')
			{
				$class .= ' divider';
			}

			if ($item->deeper)
			{
				$class .= ' deeper';
			}

			if ($item->parent)
			{
				$class .= ' parent';
			}

			if (!empty($class))
			{
				$class = ' class="' . trim($class) . '"';
			}

			echo '<li' . $class . '><span>';

				// Render the menu item.
			switch ($item->type) :
				case 'separator':
					// echo '<span class="hey-nav-separator">' . $item->title . '</span>';
					echo $item->title;
					break;
				case 'url':
				case 'component':
					if($item->menu_image) {
						echo '<a class="hey-mobile-nav-item-img" href="' . $item->flink . '">' . ( $item->menu_image ? '<span class="hey-mobile-nav-item-img"><img src="' . $item->menu_image . '" alt="' . $item->title . '" /></span>' : '') . $item->title . '</a>';
					}
					else {
						echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					}
					// require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
					break;

				default:
					echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					break;
			endswitch;


			echo '</span>';

			// The next item is deeper.
			if ($item->deeper)
			{
				echo '<button class="hey-mobile-nav-deeper-btn"><span class="bar-1">-</span><span class="bar-2">-</span></button>';
				echo '<div class="hey-mobile-nav-child">';
				echo '<ul class="hey-mobile-nav-menu-child">';
			}
			elseif ($item->shallower)
			{
				// The next item is shallower.
				echo '</li>';
				echo str_repeat('</ul></div></li>', $item->level_diff);
			}
			else
			{
				// The next item is on the same level.
				echo '</li>';
			}
		}
		?></ul>
	</div>
</div>