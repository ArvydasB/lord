<?php defined('_JEXEC') or die; ?>

<div class="hey-dropdown-nav<?php echo $class_sfx;?>">
	<nav class="hey-dropdown-menu"<?php
			$tag = '';

			if ($params->get('tag_id') != null)
			{
				$tag = $params->get('tag_id') . '';
				echo ' id="' . $tag . '"';
			}
		?>>
	<?php

		foreach ($list as $i => &$item)
		{
			$class = 'item-' . $item->id;

			if (($item->id == $active_id) OR ($item->type == 'alias' AND $item->params->get('aliasoptions') == $active_id))
			{
				$class .= ' current';
			}

			if (in_array($item->id, $path))
			{
				$class .= ' active';
			}
			elseif ($item->type == 'alias')
			{
				$aliasToId = $item->params->get('aliasoptions');

				if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
				{
					$class .= ' active';
				}
				elseif (in_array($aliasToId, $path))
				{
					$class .= ' alias parent active';
				}
			}

			if ($item->type == 'separator')	{ $class .= ' divider';	}

			if ($item->deeper) { $class .= ' deeper'; }

			if ($item->parent)	{ $class .= ' parent'; }

			if (!empty($class))	{ $class = ' class="' . trim($class) . '"';	}

			echo '<div' . $class . '>';

				// Render the menu item.
			switch ($item->type) :
				case 'separator':
					echo '<span class="hey-nav-separator">' . $item->title . '</span>';
					break;
				case 'url':
				case 'component':
						echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					break;

				default:
						echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					break;
			endswitch;

			// The next item is deeper.
			if ($item->deeper)
			{
				// echo '<div class="hey-dropdown-nav-child">';
				echo '<div class="hey-dropdown-box">';
			}
			elseif ($item->shallower)
			{
				// The next item is shallower.
				echo '</div>';
				// echo str_repeat('</ul></div></li>', $item->level_diff);
				echo str_repeat('</div></div>', $item->level_diff);
			}
			else
			{
				// The next item is on the same level.
				echo '</div>';
			}
		}
		?></nav>
</div>
