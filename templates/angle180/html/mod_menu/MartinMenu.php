<?php defined('_JEXEC') or die; ?>

<div id="mobileMenu">


 <input type="checkbox" id="toggle"/>

  <label for="toggle" id="toggle-btn"></label>
  <span class="menu-name">menu</span>
  <div class="mobile-logo">
<span class="sprite sprite-top-logo"></span>
	</div>	 
  	<button class="hamburger hamburger--3dx" type="button">
	  <span class="hamburger-box">
	    <span class="hamburger-inner"></span>
	  </span>
	</button>
  <nav data-state="close">
    
 

<div class="screen">
	<div class="circle"></div>
			<div class="menu">
	 
		<ul>

		<?php
			$search_position = JHTML::_('content.prepare', '{loadposition mobile-menu-search}');
			if($search_position != "") :
		?>
		<li id="hey-mobile-nav-search">
			<div>
				<?php echo $search_position;?>
			</div>
		</li>
		<?php endif; ?>

		<?php	

		foreach ($list as $i => &$item)
		{
			$class = 'item-' . $item->id;

			if (($item->id == $active_id) OR ($item->type == 'alias' AND $item->params->get('aliasoptions') == $active_id))
			{
				$class .= ' current';
			}

			if (in_array($item->id, $path))
			{
				$class .= ' active';
			}
			elseif ($item->type == 'alias')
			{
				$aliasToId = $item->params->get('aliasoptions');

				if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
				{
					$class .= ' active';
				}
				elseif (in_array($aliasToId, $path))
				{
					$class .= ' alias-parent-active';
				}
			}

			if ($item->type == 'separator')
			{
				$class .= ' divider';
			}

			if ($item->deeper)
			{
				$class .= ' deeper';
			}

			if ($item->parent)
			{
				$class .= ' parent';
			}

			if (!empty($class))
			{
				if ($item->menu_image){
					$class = ' class="' . trim($class) . ' nav-img"';
				} else {
					$class = ' class="' . trim($class) . '"';
				}
			}

			echo '<li' . $class . '><span>';

				// Render the menu item.
			switch ($item->type) :
				case 'separator':
					// echo '<span class="hey-nav-separator">' . $item->title . '</span>';
					echo $item->title;
					break;
				case 'url':
				case 'component':
					if($item->menu_image) {
						echo '<a href="' . $item->flink . '">' . ( $item->menu_image ? '<span><img src="' . $item->menu_image . '" alt="' . $item->title . '" /></span>' : '')  . '</a>';
					}
					else {
						echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					}
					// require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
					break;

				default:
					echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					break;
			endswitch;


			echo '</span>';

			// The next item is deeper.
			if ($item->deeper)
			{
				echo '<img class="hey-mobile-nav-deeper-btn" alt="menu-arrow" src="/images/mobile-menu-arrow.png">';
				echo '<div class="hey-mobile-nav-child">';
				echo '<ul class="hey-mobile-nav-menu-child">';
				echo '<li' . $class . '><span>';

				// Render the menu item.
			switch ($item->type) :
				case 'separator':
					// echo '<span class="hey-nav-separator">' . $item->title . '</span>';
					echo $item->title;
					break;
				case 'url':
				case 'component':
					if($item->menu_image) {
						echo '<a href="' . $item->flink . '">' . ( $item->menu_image ? '<span><img src="' . $item->menu_image . '" alt="' . $item->title . '" /></span>' : '')  . '</a>';
					}
					else {
						echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					}
					// require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
					break;

				default:
					echo '<a href="' . $item->flink . '"' . ($item->browserNav == 1 ? ' target="_blank"' : '') . '>' . $item->title . '</a>';
					break;
			endswitch;


			echo '</span></li>';

			}
			elseif ($item->shallower)
			{
				// The next item is shallower.
				echo '</li>';
				echo str_repeat('</ul></div></li>', $item->level_diff);
			}
			else
			{
				// The next item is on the same level.
				echo '</li>';
			}
		}
		?></ul>
		</div>	
		</div>	
			<div class="mobile-paint">
	</div>	            
	</nav> 



</div>