<?php

/**
 * @author      Guillermo Vargas <guille@vargas.co.cr>
 * @author      Branko Wilhelm <branko.wilhelm@gmail.com>
 * @link        http://www.z-index.net
 * @copyright   (c) 2005 - 2009 Joomla! Vargas. All rights reserved.
 * @copyright   (c) 2015 Branko Wilhelm. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


$searchScript = "function errorPageSearch () {
                    var searchWord = jQuery('#error-search-word').val();
                    jQuery('#hey-desktop-search .mod-finder-desktop-searchword').val( searchWord );
                    jQuery('#hey-desktop-search form').submit();
                }

                jQuery(document).ready( function() {
                    jQuery('#error-page-search form').on('submit', function(evt) {
                        evt.preventDefault();
                        errorPageSearch();
                    });
                });
                ";

$errorPageStyle = "
                    #error-page-header {
                        font-size: 1.5em;
                        font-weight: 600;
                        margin: 1em 0 0;
                    }

                    #error-page-subheader {
                        margin: 1.5em 0;
                    }

                    #error-page-search {
                        margin: 0 0 2em;
                    }

                    #error-page-search {
                        margin: 0 0 2em;
                    }
                    
                    #error-page-search form > * {
                        vertical-align: middle;
                    }

                    #error-search-word,
                    #error-search-word:focus,
                    #error-search-word:active {
                        border: 1px solid #DDD;
                        padding: 3px 5px;
                        border-radius: none;
                        outline: none;
                        width: 250px;
                    }

                    #error-search-btn {
                        background: transparent;
                        border: none;
                        color: #0B4044;
                        text-align: center;
                        font-size: 1.2em;
                    }

                ";


$currentMenuItem = JFactory::getApplication()->getMenu()->getActive();

$doc = JFactory::getDocument();

$doc->addScriptDeclaration($searchScript);
$doc->addStyleDeclaration($errorPageStyle);
?>

    <div id="xmap" class="sitemap<?php echo $this->pageclass_sfx ?>">
        <?php if ($currentMenuItem->alias == '404') : ?>

            <div id="error-page-header">Sorry, the page you are looking for doesn't exist or may have moved.</div>
            <div id="error-page-subheader">Please try using the search box or view our list of pages below.</div>
            <div id="error-page-search">
                <form><input id="error-search-word" placeholder="Search..." type="text"><button id="error-search-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button></form>
            </div>

        <?php endif; ?>
        <?php if ($this->params->get('show_page_heading')) : ?>
            <div class="page-header">
                <h1>
                    <?php echo $this->escape($this->params->get('page_heading')); ?>
                </h1>
            </div>
        <?php endif; ?>

        <?php if ( $this->item->params->get('showintro', 1) && !empty($this->item->introtext)  &&  $currentMenuItem->alias != '404' ) : ?>
            <div class="introtext">
                <?php echo $this->item->introtext; ?>
            </div>
        <?php endif; ?>

        <?php echo $this->displayer->printSitemap(); ?>
    </div>
   
<?php echo $this->loadTemplate('edit');