<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_finder
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_SITE . '/components/com_finder/helpers/html');

// Load the smart search component language file.
$lang = JFactory::getLanguage();
$lang->load('com_finder', JPATH_SITE);

$output = '
	<div class="search-field-wrap">
	  <div class="search-field-wrap-inner">
	  	<input type="text" name="q" id="mod-finder-desktop-searchword" size="' . $params->get('field_size', 20) . '" placeholder="Search..." value="' . htmlspecialchars(JFactory::getApplication()->input->get('q', '', 'string')) . '" />
	  </div>
	</div>
';

// $button = '<button class="btn btn-primary hasTooltip finder" type="submit" title="' . JText::_('MOD_FINDER_SEARCH_BUTTON') . '"><i class="icon-search icon-white"></i></button>';
$button = '<input id="mod-finder-desktop-searchbutton" type="image" src="/images/angle180/ico-top-search.png" alt="Search" name="Search">';
// $button = '<button id="mod-finder-desktop-searchbutton" name="Search"><span class="glyphicon glyphicon-search"></span></button>';

switch ($params->get('button_pos', 'left')) {
	case 'top' :
	$output = $button . '<br />' . $output;
	break;

	case 'bottom' :
	$output .= '<br />' . $button;
	break;

	case 'right' :
	$output .= $button;
	break;

	case 'left' :
	default :
	$output = $button . $output;
	break;
}

$search_script = '
jQuery(function($) {
	$("#mod-finder-desktop-searchbutton").click(function(event) {
			// If input hidden, show it
		var input_field = $("#mod-finder-desktop-searchword");
		if(!$("#hey-desktop-search").hasClass("searchReady")) {
			$("#mod-finder-desktop-searchword").val("");
			$("#hey-desktop-search").addClass("searchReady");
			$(".search-field-wrap").removeClass("display-none");
			event.preventDefault();
			$("#mod-finder-desktop-searchword").focus();
		}
	});

	// $("#mod-finder-desktop-searchword").focusout(function (event) {
 // 		if(!$("#mod-finder-desktop-searchbutton").is(":hover")){
 // 			$("#hey-desktop-search").removeClass("searchReady");
 // 		}
 // 	});

	$(".mod-finder-searchform").on("submit", function(e){
		e.stopPropagation();
		var $advanced = $("#mod-finder-advanced");
				// Disable select boxes with no value selected.
		if ( $advanced.length) {
			$advanced.find("select").each(function(index, el) {
				var $el = $(el);
				if(!$el.val()){
					$el.attr("disabled", "disabled");
				}
			});
		}
	});
	// $(".autocomplete-suggestion").on("click", function(e){
	// 	$(".mod-finder-searchform").submit();
	// });
	';


	JHtml::_('script', 'media/jui/js/jquery.autocomplete.js', false, false, false, false, true);

	$search_script .= "
	var suggest = jQuery('#mod-finder-desktop-searchword').autocomplete({
		serviceUrl: '" . JRoute::_('index.php?option=com_finder&task=suggestions.suggest&format=json&tmpl=component') . "',
		paramName: 'q',
		minChars: 1,
		maxHeight: 400,
		width: 300,
		zIndex: 999999,
		containerClass: 'autocomplete-suggestions-desktop',
		deferRequestBy: 500,
		onSelect: function(event, ui){
			$('#mod-finder-desktop-searchword').val(event.value);
			
				$('#mod-finder-desktop-searchbutton').click();
			
		}
	});";
$search_script .= '});';

// $search_script = '
// jQuery(function($) {
// 	$("#mod-finder-desktop-searchbutton").click(function(event) {
// 			// If input hidden, show it
// 		var input_field = $("#mod-finder-desktop-searchword");
// 		if(!$("#hey-desktop-search").hasClass("searchReady")) {
// 			$("#hey-desktop-search").addClass("searchReady");
// 			event.preventDefault();
// 			$("#mod-finder-desktop-searchword").focus();
// 		}else {
// 				// If input is empty, hide it
// 			if($("#mod-finder-desktop-searchword").val() == "") {
// 				$("#hey-desktop-search").removeClass("searchReady");
// 				event.preventDefault();
// 			}
// 		}
// 	});

// 	$("#mod-finder-desktop-searchword").focusout(function (event) {
// 		if(!$("#mod-finder-desktop-searchbutton").is(":hover")){
// 			$("#hey-desktop-search").removeClass("searchReady");
// 		}
// 	});

// 	$(".mod-finder-searchform").on("submit", function(e){
// 		e.stopPropagation();
// 		var $advanced = $("#mod-finder-advanced");
// 				// Disable select boxes with no value selected.
// 		if ( $advanced.length) {
// 			$advanced.find("select").each(function(index, el) {
// 				var $el = $(el);
// 				if(!$el.val()){
// 					$el.attr("disabled", "disabled");
// 				}
// 			});
// 		}
// 	});
// });';

$doc = JFactory::getDocument();
$doc->addScriptDeclaration( $search_script );
?>

<div id="hey-desktop-search">
	<form action="<?php echo JRoute::_($route); ?>" method="get" class="mod-finder-searchform form-search">
		<div class="finder">
			<?php
			echo $output;
			?>

			<?php $show_advanced = $params->get('show_advanced'); ?>
			<?php if ($show_advanced == 2) : ?>
				<br />
				<a href="<?php echo JRoute::_($route); ?>"><?php echo JText::_('COM_FINDER_ADVANCED_SEARCH'); ?></a>
			<?php elseif ($show_advanced == 1) : ?>
				<div id="mod-finder-desktop-advanced">
					<?php echo JHtml::_('filter.select', $query, $params); ?>
				</div>
			<?php endif; ?>
			<?php echo modFinderHelper::getGetFields($route, (int) $params->get('set_itemid')); ?>
		</div>
	</form>
</div>
