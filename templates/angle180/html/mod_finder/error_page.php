<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_finder
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_SITE . '/components/com_finder/helpers/html');

// Load the smart search component language file.
$lang = JFactory::getLanguage();
$lang->load('com_finder', JPATH_SITE);

$output = '<input type="text" name="q" class="mod-finder-desktop-searchword" size="' . $params->get('field_size', 20) . '" placeholder="Search..." value="' . htmlspecialchars(JFactory::getApplication()->input->get('q', '', 'string')) . '" />';

// $button = '<button class="btn btn-primary hasTooltip finder" type="submit" title="' . JText::_('MOD_FINDER_SEARCH_BUTTON') . '"><i class="icon-search icon-white"></i></button>';
$button = '<input id="mod-finder-desktop-searchbutton" type="image" src="images/angle180/ico-search.png" alt="Search" name="Search">';
// $button = '<button id="mod-finder-desktop-searchbutton" name="Search"><span class="glyphicon glyphicon-search"></span></button>';

switch ($params->get('button_pos', 'left')) {
	case 'top' :
	$output = $button . '<br />' . $output;
	break;

	case 'bottom' :
	$output .= '<br />' . $button;
	break;

	case 'right' :
	$output .= $button;
	break;

	case 'left' :
	default :
	$output = $button . $output;
	break;
}

$search_script = '
jQuery(function($) {
	$("#mod-finder-desktop-searchbutton").click(function(event) {
			// If input hidden, show it
		var input_field = $("#hey-desktop-search .mod-finder-desktop-searchword");
		if(!$(input_field).hasClass("searchReady")) {
			$(input_field).addClass("searchReady");
			event.preventDefault();
			$(".mod-finder-desktop-searchword").focus();
		}else {
				// If input is empty, hide it
			if($(".mod-finder-desktop-searchword").val() == "") {
				$(".mod-finder-desktop-searchword").removeClass("searchReady");
				event.preventDefault();
			}
		}
	});

	$(".mod-finder-desktop-searchword").focusout(function (event) {
		if(!$("#mod-finder-desktop-searchbutton").is(":hover")){
			$(".mod-finder-desktop-searchword").removeClass("searchReady");
		}
	});

	$(".mod-finder-searchform").on("submit", function(e){
		e.stopPropagation();
		var $advanced = $("#mod-finder-advanced");
				// Disable select boxes with no value selected.
		if ( $advanced.length) {
			$advanced.find("select").each(function(index, el) {
				var $el = $(el);
				if(!$el.val()){
					$el.attr("disabled", "disabled");
				}
			});
		}
	});
});';

$doc = JFactory::getDocument();
$doc->addScriptDeclaration( $search_script );
?>
<div id="hey-desktop-search">
	<form action="<?php echo JRoute::_($route); ?>" method="get" class="mod-finder-searchform form-search">
		<div class="finder">
			<?php
			echo $output;
			?>

			<?php $show_advanced = $params->get('show_advanced'); ?>
			<?php if ($show_advanced == 2) : ?>
				<br />
				<a href="<?php echo JRoute::_($route); ?>"><?php echo JText::_('COM_FINDER_ADVANCED_SEARCH'); ?></a>
			<?php elseif ($show_advanced == 1) : ?>
				<div id="mod-finder-desktop-advanced">
					<?php echo JHtml::_('filter.select', $query, $params); ?>
				</div>
			<?php endif; ?>
			<?php echo modFinderHelper::getGetFields($route, (int) $params->get('set_itemid')); ?>
		</div>
	</form>
</div>
