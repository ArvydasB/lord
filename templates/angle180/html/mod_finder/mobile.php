<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_finder
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$lang->load('com_finder', JPATH_SITE);

$suffix = $params->get('moduleclass_sfx');
$output = '<input type="text" name="q" id="mod-finder-mobile-searchword" class="mod-finder-searchword search-query input-medium" size="' . $params->get('field_size', 20) . '" placeholder="Search..." value="' . htmlspecialchars(JFactory::getApplication()->input->get('q', '', 'string')) . '" />';

if ($params->get('show_button'))
{
	$button = '<span id="mod-finder-mobile-searchbutton">Search...</span>';
	// $button = '<input class="mod-finder-searchbutton" type="image" src="images/angle180/icons/ico-Search.png" alt="Search" name="Search">';
	switch ($params->get('button_pos', 'left'))
	{
		case 'top' :
		$output = $button . '<br />' . $output;
		break;

		case 'bottom' :
		$output .= '<br />' . $button;
		break;

		case 'right' :
		$output .= $button;
		break;

		case 'left' :
		default :
		$output = $button . $output;
		break;
	}
}

$search_script = '
jQuery(function($) {
	$("#mod-finder-mobile-searchbutton").click(function(event) {
			// If input hidden, show it
		var input_field = $("#mod-finder-mobile-searchword");
		if(!$("#hey-mobile-search").hasClass("searchReady")) {
			$("#mod-finder-mobile-searchword").val("");
			$("#hey-mobile-search").addClass("searchReady");
			$(".search-field-wrap").removeClass("display-none");
			event.preventDefault();
			$("#mod-finder-mobile-searchword").focus();
		}
	});

	// $("#mod-finder-mobile-searchword").focusout(function (event) {
 // 		if(!$("#mod-finder-mobile-searchbutton").is(":hover")){
 // 			$("#hey-mobile-search").removeClass("searchReady");
 // 		}
 // 		// if ($(".autocomplete-suggestion").length > 0){
 // 		// 	$(".autocomplete-suggestion").remove();
 // 		// }
 // 	});

	';
	JHtml::_('script', 'media/jui/js/jquery.autocomplete.js', false, false, false, false, true);

	$search_script .= "
	var suggest = jQuery('#mod-finder-mobile-searchword').autocomplete({
		serviceUrl: '" . JRoute::_('index.php?option=com_finder&task=suggestions.suggest&format=json&tmpl=component') . "',
		paramName: 'q',
		minChars: 1,
		maxHeight: 400,
		width: 300,
		zIndex: 999999,
		containerClass: 'autocomplete-suggestions-mobile',
		deferRequestBy: 500,
		onSelect: function(event, ui){
			$('#mod-finder-mobile-searchword').val(event.value);
			$('#mod-finder-mobile-searchbutton').parent().parent().submit();
		}
	});";
$search_script .= '});';

// $search_script = '
// jQuery(function($) {
// 	$("#mod-finder-mobile-searchbutton").click(function(event) {
// 		if($("#mod-finder-mobile-searchword").val() == "") {
// 			$("#mod-finder-mobile-searchword").focus();
// 			event.preventDefault();
// 		}
// 	});
// });';

$doc = JFactory::getDocument();
$doc->addScriptDeclaration( $search_script );
?>

<li id="hey-mobile-search">
	<form action="<?php echo JRoute::_($route); ?>" method="get" class="mod-finder-searchform form-search">
		<div class="finder<?php echo $suffix; ?>">
			<?php
		// Show the form fields.
			echo $output;
			?>

			<?php $show_advanced = $params->get('show_advanced'); ?>
			<?php if ($show_advanced == 2) : ?>
				<br />
				<a href="<?php echo JRoute::_($route); ?>"><?php echo JText::_('COM_FINDER_ADVANCED_SEARCH'); ?></a>
			<?php elseif ($show_advanced == 1) : ?>
				<div id="mod-finder-advanced">
					<?php echo JHtml::_('filter.select', $query, $params); ?>
				</div>
			<?php endif; ?>
			<?php echo modFinderHelper::getGetFields($route, (int) $params->get('set_itemid')); ?>
		</div>
	</form>
</li>
