<?php
/**
 * @copyright	Copyright (C) 2015 Angle180 LLC. All rights reserved.
 * @license		Commercial Template. Do not use without written permission on Angle180 LLC.
 */

defined('_JEXEC') or die;


?>
<div class="testimonials-list-page<?php echo $this->pageclass_sfx; ?>">
	<div class="testimonial">
		<?php foreach ($this->items as $key => $testimonial) : ?>
			<div class="testimonial-inner">
				<h3><?php echo $testimonial->title; ?></h3>
				<div class="testimonial-content"><?php echo $testimonial->introtext; ?></div>				
			</div>
		<?php endforeach; ?>
	</div>

</div>
