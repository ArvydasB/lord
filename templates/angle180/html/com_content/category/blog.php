<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');
?>
<div class="item-page">
<span class="inside-title">Blog</span>
<span class="sub-heading">Read helpful tips and tricks about painting</span>
</div>
<div class="blog<?php echo $this->pageclass_sfx; ?>" itemscope itemtype="http://schema.org/Blog">
	
	<div class="blogpost-content">
		<?php
			foreach ($this->items as $key => $item) :
				$json_img       = json_decode($item->images);
				$img            = $json_img->{"image_intro"};
				$img_alt        = $json_img->{"image_intro_alt"};
				$img_caption    = $json_img->{"image_intro_caption"};
				$title          = $item->title;
				$date           = date_create($item->created);
				$intro          = explode(" ", strip_tags($item->introtext));
				$item_url       = trim( JURI::base(), '/' ) . JRoute::_( ContentHelperRoute::getArticleRoute($item->id, $item->catid) );

		?>
			
			<div class="latest-blog-post">
                <div class="post-title">
                    <a href="<?php echo $item_url; ?>"><?php echo $title; ?></a>
                </div>
                <div class="post-date sprite sprite-calendar-yellow">
                    <?php echo date_format($date, 'm/d/Y'); ?>
                </div>
                <div class="post-img">
                     <a href="<?php echo $item_url; ?>"><img src="/<?php echo $img; ?>" alt="<?php echo $img_alt; ?>"></a>
                    <!-- <div class="img-caption"><?php //echo $img_caption; ?></div> -->
                </div>
                <div class="post-intro">
                    <?php //length of intro
						for ($i = 0; ($i < 100 && $i < count($intro)); $i++) {
						  echo $intro[$i] . " ";
						}
					?>...
                </div>

                <div class="post-readmore">                
                    <a href="<?php echo $item_url; ?>">Continue reading</a>
                    <div class="read-more-sprite">
	                    <div class="sprite sprite-link-arrow">
	               		 </div>
               		 </div>
                </div>

            </div>

		<?php endforeach; ?>
	</div>

	<?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
		<div class="blog-post pagination">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<!-- <p class="counter pull-right"> <?php //echo $this->pagination->getPagesCounter(); ?> </p> -->
			<?php endif; ?>
			<?php echo $this->pagination->getPagesLinks(); ?> </div>
	<?php endif; ?>
</div>
