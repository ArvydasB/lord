<?php defined( '_JEXEC' ) or die( 'Unauthorized Access' ); ?>
<?php JFactory::getDocument()->setMetaData( 'robots' , 'noindex,follow' ); ?>

<div id="blog-body">
	<div id="blog-category-detail">
		<h3><?php echo $archiveTitle; ?></h3>
	</div>

	<div id="blog-posts" class="archive-list">
	<?php
	if( $data )
	{
		foreach ( $data as $row )
		{
			// find first paragraph
			preg_match_all("/<p.*?>(.*)<\/p>/", $row->text, $matches);
			if(isset($matches[0][0])) {
				$intro = strip_tags($matches[0][0]);
			}else {
				// If paragrahps not used
				$intro = strip_tags( $row->text );
			}

			//shorten if needed
			if( strlen($intro) > 599 ) {
				if (preg_match('/^.{1,599}\b/s', $intro, $matches)) {
					$intro = $matches[0].'&hellip;';
				}
			}

			// print_r($row);
			?>
			<div class="blog-post" itemscope itemtype="http://schema.org/Blog">
				<div class="blog-post-in">
					<h3 itemprop="name">
						<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$row->id); ?>" itemprop="url"><?php echo $row->title; ?></a>
					</h3>

					<div class="blog-post-meta">
							<?php echo $this->fetch( 'blog.meta.php' , array( 'row' => $row, 'postedText' => JText::_( 'COM_EASYBLOG_POSTED' ) ) ); ?>
					</div>
					
					<div class="blog-post-content">
						<?php echo $intro; ?>
					</div>
					<div class="blog-post-info">
						<?php
							// Category
							if( $this->getParam( 'show_category' ) ) { 
								$categoryName   = isset($row->category) ? JText::_( $row->category ) : $row->getCategoryName();
								echo 'Posted in <a href="' . EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id=' . $row->category_id ) . '">' . $categoryName . '</a>';
							}
							// Tags
							if( $this->getParam( 'show_tags' ) && $this->getParam( 'show_tags_frontpage' , true ) ){ 
								echo $this->fetch( 'tags.item.php' , array( 'tags' => $row->tags ) );
							} 
						?>
					</div>
					<div class="blog-post-readmore">
						<?php if( $row->readmore ) { ?>
							<?php echo $this->fetch( 'blog.readmore.php' , array( 'row' => $row ) ); ?>
						<?php } ?>
					</div>
				</div>
			</div>

			<?php
			$isMineBlog = EasyBlogHelper::isMineBlog($row->created_by, $my->id);

			$team   = '';
			if( isset($row->team_id) )
			{
			$team	= ( !empty( $row->team_id )) ? $row->team_id : '';
			}

			$blogger    = EasyBlogHelper::getTable( 'Profile', 'Table');
			$blogger->load( $row->created_by );
			continue;
	?>
			<li id="entry-<?php echo $row->id; ?>" class="post-wrapper<?php echo !empty( $row->source ) ? ' micro-' . $row->source : ' micro-post';?>">
				<time datetime="<?php echo $this->formatDate( '%Y-%m-%d' , $row->created ); ?>">
					<?php echo $this->formatDate( $system->config->get('layout_dateformat') , $row->created ); ?>
				</time>
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$row->id); ?>"><?php echo $row->title; ?></a>
				<?php if( $row->isFeatured ) { ?><b class="tag-featured"><?php echo Jtext::_('COM_EASYBLOG_FEATURED_FEATURED'); ?></b><?php } ?>
			</li>
				<?php } ?>
			<?php } else { ?>
				<li>
					<div class="eblog-message info"><?php echo $emptyPostMsg;?></div>
				</li>
	<?php } ?>
	</div>
	<?php if ( $pagination ) : ?>
		<div class="pagination"><?php echo $pagination;?></div>
	<?php endif; ?>
</div>
