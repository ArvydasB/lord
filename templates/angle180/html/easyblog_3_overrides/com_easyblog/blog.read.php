<?php defined('_JEXEC') or die('Restricted access'); ?>

<!-- EasyBlog wrappers -->
<div id="blog-body" class="blog-read" itemscope itemtype="http://schema.org/Blog">
	<!-- Entry wrapper -->
	<div class="blog-post">
		<h1 id="title-<?php echo $blog->id; ?>" class="blog-title<?php echo ($isFeatured) ? ' featured-item' : '';?> rip" itemprop="name"><?php echo $blog->title; ?></h1>

		<div class="blog-post-meta">
			<?php echo $this->fetch( 'blog.meta.read.php' , array( 'row' => $blog , 'postedText' => JText::_( 'COM_EASYBLOG_POSTED' ) ) ); ?>
		</div>

		<div class="blog-post-content">


			<?php if( $blog->getImage() && $this->getParam( 'blogimage_entry') ){ ?>
			<div class="blog-image-container">
				<a class="easyblog-thumb-preview" href="<?php echo $blog->getImage()->getSource('original');?>" title="<?php echo $this->escape( $blog->title );?>">
					<img src="<?php echo $blog->getImage()->getSource( 'entry' );?>" class="blog-image" alt="<?php echo $this->escape( $blog->title );?>" /></a>
				</div>
				<?php } ?>
			</div>
			
			<?php echo $blog->content; ?>

			<div class="blog-post-info">
				<?php
					if( $this->getParam( 'show_category' )) {
						echo 'Posted in <a href="' . EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id=' . $blog->category_id ) . '">' . $blog->getCategoryName() . '</a>';
					}
					if( $this->getParam( 'show_tags' ) ) {
						echo $this->fetch( 'tags.item.php' , array( 'tags' => $tags ) );
					} 
				?>
			</div>

			<div class="blog-post-social">
				<?php if( in_array( $system->config->get( 'main_socialbutton_position' ) , array( 'top' , 'left' , 'right' ) ) ){ ?>
				<?php echo EasyBlogHelper::showSocialButton($blog); ?>
				<?php } ?>
			</div>
		</div>
		<div class="blog-navigation">
			<?php echo $this->fetch( 'blog.read.navigation.php' , array( 'nextLink' => $nextLink , 'prevLink' => $prevLink ) ); ?>
		</div>
	</div>
