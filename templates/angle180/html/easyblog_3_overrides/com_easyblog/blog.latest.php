<?php
/**
  * @copyright	Copyright (C) 2015 Heymaan, Inc. All rights reserved.
 */
defined('_JEXEC') or die('Restricted access');
?>

<div id="blog-body">
	<div id="blog-posts">

		<?php if( $data ){ ?>
			<?php foreach( $data as $row ){ ?>
				<?php
					// Password protected theme files
					if( $system->config->get( 'main_password_protect' ) && !empty( $row->blogpassword ) ){ ?>
					<?php echo $this->fetch( 'blog.item.protected.php' , array( 'row' => $row ) ); ?>
				<?php } else {
					// Normal post theme files
					?>
					<?php echo $this->fetch( 'blog.item'. EasyBlogHelper::getHelper( 'Sources' )->getTemplateFile( $row->source ) . '.php' , array( 'row' => $row ) ); ?>

				<?php } ?>
			<?php } ?>
		<?php } else { ?>

			<!-- Empty listing notice -->
			<div class="mtl"><?php echo JText::_('COM_EASYBLOG_NO_BLOG_ENTRY');?></div>

		<?php } ?>
	</div>

	<?php if( $pagination ){?>
		<!-- Pagination items -->
		<div class="pagination"><?php echo $pagination;?></div>
	<?php } ?>
</div>
