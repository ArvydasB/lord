<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php JFactory::getDocument()->setMetaData( 'robots' , 'noindex,follow' ); ?>

<div id="blog-body">
	<?php if( $this->getParam('show_category_header', '1' ) ){ ?>
	<div id="blog-category-detail">
		<h3>Category Archives: <?php echo JText::_( $category->title ); ?></h3>
	</div>
	<?php } ?>

	<?php if ( $teamBlogCount > 0) { ?>
	<div class="eblog-message info"><?php echo $this->getNouns( 'COM_EASYBLOG_CATEGORIES_LISTINGS_TEAMBLOG_COUNT' , $teamBlogCount , true ); ?></div>
	<?php } ?>

	<div id="blog-posts">
		<?php if($system->my->id == 0 && $category->private == 1 ){ ?>
			<div class="eblog-message warning"><?php echo JText::_('COM_EASYBLOG_CATEGORIES_FOR_REGISTERED_USERS_ONLY');?></div>
		<?php } else if( $category->private == 2 && !$allowCat ) { ?>
		<div class="eblog-message warning"><?php echo JText::_('COM_EASYBLOG_CATEGORIES_NOT_ALLOWED');?></div>
		<?php } else { ?>
		<?php if( $blogs ) { ?>
		<?php foreach( $blogs as $row ){ ?>
		<?php if( $system->config->get( 'main_password_protect' ) && !empty( $row->blogpassword ) ){ ?>
		<!-- Password protected theme files -->
		<?php echo $this->fetch( 'blog.item.protected.php' , array( 'row' => $row ) ); ?>
		<?php } else { ?>
		<!-- Normal post theme files -->
		<?php echo $this->fetch( 'blog.item'. EasyBlogHelper::getHelper( 'Sources' )->getTemplateFile( $row->source ) . '.php' , array( 'row' => $row ) ); ?>
		<?php } ?>
		<?php } ?>
		<?php } else { ?>
		<div class="eblog-message warning"><?php echo JText::_('COM_EASYBLOG_NO_BLOG_ENTRY');?></div>
		<?php } ?>

		<?php if( $pagination ){ ?>
			<div class="pagination"><?php echo $pagination;?></div>
		<?php } ?>
		<?php } ?>
	</div>

</div>
