<?php defined( '_JEXEC' ) or die( 'Unauthorized Access' ); ?>
<?php JFactory::getDocument()->setMetaData( 'robots' , 'noindex,follow' ); ?>

<div id="blog-body">
	<div id="blog-category-detail">
		<h3>Author Archives: <?php echo $blogger->getName(); ?></h3>
	</div>

	<div id="blog-posts">
		<?php
		if(!empty($blogs))
		{
			foreach ($blogs as $row)
			{
				?>
				<?php if( $system->config->get( 'main_password_protect' ) && !empty( $row->blogpassword ) ){ ?>
				<!-- Password protected theme files -->
				<?php echo $this->fetch( 'blog.item.protected.php' , array( 'row' => $row ) ); ?>
				<?php } else { ?>
				<!-- Normal post theme files -->
				<?php echo $this->fetch( 'blog.item'. EasyBlogHelper::getHelper( 'Sources' )->getTemplateFile( $row->source ) . '.php' , array( 'row' => $row ) ); ?>
				<?php } ?>
				<?php
			}
		}
		else
		{
			?>
			<div class="eblog-message info"><?php echo JText::sprintf('COM_EASYBLOG_BLOGGERS_NO_POST_YET' , $blogger->getName() ); ?></div>
			<?php
		}
		?>

		<?php if ( $pagination ) : ?>
			<div class="pagination"><?php echo $pagination;?></div>
		<?php endif; ?>
	</div>
</div>
