<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php JFactory::getDocument()->setMetaData( 'robots' , 'noindex,follow' ); ?>

<div id="blog-body">
	<div id="blog-category-detail">
		<h3>Tag Archives: <?php echo JText::_( $tag->title ); ?></h3>
	</div>

	<div id="blog-posts">
		<?php if( $rows ){ ?>
		<?php foreach( $rows as $row ){ ?>
		<?php echo $this->fetch( 'blog.item'. EasyBlogHelper::getHelper( 'Sources' )->getTemplateFile( $row->source ) . '.php' , array( 'row' => $row ) ); ?>
		<?php } ?>
		<?php } else { ?>
		<div class="mtl"><?php echo JText::sprintf( 'COM_EASYBLOG_TAGS_NO_ENTRIES_TAGGED' , $tag->title );?></div>
		<?php } ?>

		<?php if( $pagination ){ ?>
		<div class="pagination"><?php echo $pagination->getPagesLinks(); ?></div>
		<?php } ?>
	</div>
</div>
