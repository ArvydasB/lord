<?php defined('_JEXEC') or die('Restricted access'); ?>

<!-- Post item wrapper -->
<div class="blog-post" itemscope itemtype="http://schema.org/Blog">
	<div class="blog-post-in">

		<h2 itemprop="name">
			<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$row->id); ?>" itemprop="url"><?php echo $row->title; ?></a>
		</h2>

		<div class="blog-post-meta">
				<?php echo $this->fetch( 'blog.meta.php' , array( 'row' => $row, 'postedText' => JText::_( 'COM_EASYBLOG_POSTED' ) ) ); ?>
		</div>

		<div class="blog-post-content">
				<?php
					// Blog
					if( $row->getImage() && $this->getParam( 'blogimage_frontpage') ) {
						?><a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id='.$row->id); ?>" class="blog-image">
							<img class="blog-post-img" src="<?php echo $row->getImage()->getSource( 'frontpage' );?>" alt="<?php echo $this->escape( $row->title );?>" />
						</a><?php
					}
					echo $row->text;
				?>
		</div>

		<div class="blog-post-info">
			<?php
				// Category
				if( $this->getParam( 'show_category' ) ) { 
					$categoryName   = isset($row->category) ? JText::_( $row->category ) : $row->getCategoryName();
					echo 'Posted in <a href="' . EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id=' . $row->category_id ) . '">' . $categoryName . '</a>';
				}
				// Tags
				if( $this->getParam( 'show_tags' ) && $this->getParam( 'show_tags_frontpage' , true ) ){ 
					echo $this->fetch( 'tags.item.php' , array( 'tags' => $row->tags ) );
				} 
			?>
		</div>
		<div class="blog-post-social">
			<?php
				if( in_array( $system->config->get( 'main_socialbutton_position' ) , array( 'top' , 'left' , 'right' ) ) ) {
					echo EasyBlogHelper::showSocialButton( $row , true );
				}
			?>
		</div>

		<div class="blog-post-readmore">
			<?php if( $row->readmore ) { ?>
				<?php //echo $this->fetch( 'blog.readmore.php' , array( 'row' => $row ) ); ?>
			<?php } ?>
		</div>
	</div>
</div>
