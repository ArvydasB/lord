<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
	// Recursevly print categories and their childs
	function print_category($categories, $menu_id) {
		foreach ($categories as $key => $category) {
			echo '<li><a href="' . EasyBlogRouter::_('index.php?option=com_easyblog&view=categories&layout=listings&id='.$category->id . '&Itemid=' . $menu_id ) . '">' . JText::_( $category->title ) . '</a>';
			if(  sizeof($category->childs)  ) {
				echo '<ul>';
					print_category($category->childs, $menu_id);
				echo '</ul>';
			}
			echo '</li>';
		}
	}
?>
<div class="blog-sidebar categories-list<?php echo $params->get( 'moduleclass_sfx' ) ?>">
	<ul>
		<?php
		if(!empty($categories)) {
			print_category($categories, $params['menuitemid']);
		} else {
			echo JText::_('MOD_EASYBLOGCATEGORIES_NO_CATEGORY'); 
		}
		?>
	</ul>
</div>
