<?php defined('_JEXEC') or die('Restricted access'); ?>
<div class="blog-sidebar resent-posts-list <?php echo $params->get( 'moduleclass_sfx' ) ?>">
	<?php if( $posts ) : ?>
		<ul>

		<?php
			foreach( $posts as $post ) : ?>

			<li class="item">
				<a href="<?php echo EasyBlogRouter::_('index.php?option=com_easyblog&view=entry&id=' . $post->id . $itemId ); ?>"><?php echo $post->title;?></a>
			</li>

		<?php endforeach; ?>

		</ul>

	<?php endif; ?>
</div>
