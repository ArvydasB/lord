<?php defined('_JEXEC') or die('Restricted access'); ?>

<?php
$menuItemId = modEasyBlogTagCloudHelper::_getMenuItemId($params);
$layout     = $params->get( 'layout', 'tagcloud');

?>
<div class="blog-sidebar blog-tagcloud<?php echo $params->get( 'moduleclass_sfx' ) ?>">
	<?php
		if( !empty( $tagcloud ) )
		{
			require( JModuleHelper::getLayoutPath('mod_easyblogtagcloud', 'default_' . $layout ) );
		}
		else
		{
			echo JText::_('MOD_EASYBLOGTAGCLOUD_NO_TAG');
		}
	?>
</div>
