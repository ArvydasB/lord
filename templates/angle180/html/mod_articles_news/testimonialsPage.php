<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?> 

<div class="testimonials-list-page">
	<div class="testimonial">
		<?php foreach ($list as $testimonial) : ?>

			<?php
						$authorstart = strpos($testimonial->introtext, '[author]');
						$authorend = strpos($testimonial->introtext, '[/author]');
						$quotestart = strpos($testimonial->introtext, '[quote]');
						$quoteend = strpos($testimonial->introtext, '[/quote]');
						$projectstart = strpos($testimonial->introtext, '[project]');
						$projectend = strpos($testimonial->introtext, '[/project]');
						$starsstart = strpos($testimonial->introtext, '[stars]');
						$starsend = strpos($testimonial->introtext, '[/stars]');
						$reviewstart = strpos($testimonial->introtext, '[review]');


						(int)$stars = substr($testimonial->introtext, $starsstart + 7, $starsend - $starsstart - 7);
						$review = substr($testimonial->introtext, $reviewstart + 8);						
						$project = substr($testimonial->introtext, $projectstart + 9, $projectend - $projectstart - 9);
						$author = substr($testimonial->introtext, $authorstart + 8, $authorend - $authorstart - 8);
						$quote = substr($testimonial->introtext, $quotestart + 7, $quoteend - $quotestart - 7);
			?>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 testimonials">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding testimonial-blk">
					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 nopadding author-text">
						<div class="col-lg-4 col-md-4 col-xs-3 col-sm-3 nopadding">
								<div class="sprite sprite-avatar">
															</div>
						</div>
						<div class="col-lg-8 col-md-8 col-xs-9 col-sm-9 nopadding">
	<div class="author "><?php echo $author; ?></div>
						<div class="review-stars ">
								<?php for ($i = 0; $i < $stars; $i++){
										echo '<div class="inlinedisplay sprite sprite-star"></div>';
									}
									if ($stars != 5){
										for ($i = $stars; $i < 5; $i++){
											echo '<div class="inlinedisplay sprite sprite-star-empty"></div>';
										}
									}
								?>
						</div>
						</div>
					
					</div>

					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 nopadding quote">
						<?php echo $quote; ?>
					</div>

					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 nopadding project">
						<?php echo $project; ?>
					</div>

					<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 nopadding review">
						<?php echo $review; ?>
					</div>
				</div>
			</div>
		
		<?php endforeach; ?>
	</div>

</div>