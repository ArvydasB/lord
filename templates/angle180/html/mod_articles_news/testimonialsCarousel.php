<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?> 
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 section4-review">
<h4 class="review-title">Client reviews</h4>
</div>
</div>
</div>


<div id="testimonials-carousel" class="carousel slide" data-ride="carousel" data-interval="10000">
	<!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<?php
		// print_r($list);
		$fist = true;
		foreach ($list as $item) : ?>
		<?php if($item->featured == 0) continue; ?>
		<div class="item<?php if($fist) {echo ' active'; $fist=false;}?>">
			<div class="carousel-caption">
				<div class="container">
					<div class="testimonial-content-box">					
						<?php 
						$authorstart = strpos($item->introtext, '[author]');
						$authorend = strpos($item->introtext, '[/author]');
						$starsstart = strpos($item->introtext, '[stars]');
						$starsend = strpos($item->introtext, '[/stars]');
						$reviewstart = strpos($item->introtext, '[review]');
						$author = substr($item->introtext, $authorstart + 8, $authorend - $authorstart - 8);
						(int)$stars = substr($item->introtext, $starsstart + 7, $starsend - $starsstart - 7);
						$review = substr($item->introtext, $reviewstart + 8);
						?>
						<div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 nopadding">
							<div class="sprite sprite-avatar">
							</div>
						</div>
						
						<div class="col-lg-3 col-md-4 col-sm-5 col-xs-10 nopadding author-stars">
							<div class="inlinedisplay review-author">
							<?php echo $author; ?>
							</div>
							<div class="review-stars">
							<?php for ($i = 0; $i < $stars; $i++){
									echo '<div class="inlinedisplay sprite sprite-star"></div>';
								}
								if ($stars != 5){
									for ($i = $stars; $i < 5; $i++){
										echo '<div class="inlinedisplay sprite sprite-star-empty"></div>';
									}
								}
							?>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding review">
							<?php	echo $review; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<div class="carousel-control-box">
		<div class="container">
			<a class="sprite sprite-right carousel-control" href="#testimonials-carousel" role="button" data-slide="next">
				<span class="sr-only">Next</span>
			</a>
			<a class="sprite sprite-left carousel-control" href="#testimonials-carousel" role="button" data-slide="prev">
				<span class="sr-only">Previous</span>
			</a>
		</div>
	</div>
</div>
