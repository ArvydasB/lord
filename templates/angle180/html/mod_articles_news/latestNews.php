<?php
defined('_JEXEC') or die;
?>

<div class="latest-news container nopadding">
	<div class="row">
		<?php foreach ($list as $item) : ?>
			<?php
			// print_r($item);
				$title = $item->title;

				// if( strlen($title) > 47 )
				// if (preg_match('/^.{1,47}\b/s', $title, $matches)) {
				// 	$title = $matches[0].'&hellip;';
				// }


				preg_match("/<p>(.*)<\/p>/", $item->introtext, $matches);
				$intro = strip_tags($matches[1]);

				if( strlen($intro) > 220 )
				if (preg_match('/^.{1,220}\b/s', $intro, $matches)) {
					$intro = $matches[0].'&hellip;';
				}
			?>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="news-post">
					<h4 class="news-title"><a href="<?php echo $item->link; ?>"><?php echo $title; ?></a></h4>
					<div class="news-date"><?php echo date("F d, o", strtotime($item->created)); ?></div>
					<div class="news-intro"><?php echo $intro; ?></div>
					<div class="news-readmore"><a href="<?php echo $item->link; ?>">Read More</a></div>

				</div>
			</div>
		<?php endforeach; ?>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="news-readall">
				<a href="index.php?option=com_content&view=category&layout=blog&id=9&Itemid=107">See all news</a> 
			</div>
		</div>
	</div>
</div>