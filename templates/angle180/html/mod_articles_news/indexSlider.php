<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?> 

<div id="quote-box" class="carousel container slide carousel-fade" data-ride="carousel" data-interval="8000">
	<!-- Wrapper for slides -->
	<div class="carousel-inner col-lg-12 col-md-12 col-sm-12 col-xs-12" role="listbox">
		
			<div class="sprite sprite-quote">
						</div>	
		<?php
		// print_r($list);
		$fist = true;
		foreach ($list as $item) : ?>
		<?php if($item->featured == 0) continue; ?>
		<div class="item<?php if($fist) {echo ' active'; $fist=false;}?>">		
							
						<?php 
						$authorstart = strpos($item->introtext, '[author]');
						$authorend = strpos($item->introtext, '[/author]');
						$quotestart = strpos($item->introtext, '[quote]');
						$quoteend = strpos($item->introtext, '[/quote]');
						$quote = substr($item->introtext, $quotestart + 7, $quoteend - $quotestart - 7);
						$projectstart = strpos($item->introtext, '[project]');
						$projectend = strpos($item->introtext, '[/project]');
						$project = substr($item->introtext, $projectstart + 9, $projectend - $projectstart - 9);

						$author = substr($item->introtext, $authorstart + 8, $authorend - $authorstart - 8);
						?>
						<div class="quote-heading">
							<?php	echo $quote; ?>
						</div>						
						<div class="quote-text">
							<span class="blockdisplay name">
							<?php echo $author; ?>
							</span>
						</div>
						<div class="quote-project">
							<?php echo $project; ?>
						</div>

						
			</div>
	<?php endforeach; ?>
</div>
</div>
