<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div id="testimonials-page" >

	<?php
		// print_r($list);
	foreach ($list as $item) : ?>
	<?php if($item->featured == 0) continue; ?>
	<div class="testimonials-page-item">
		<?php echo strip_tags($item->introtext, '<span>'); ?>
	</div>
<?php endforeach; ?>
</div>


