<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>



<div id="latest-news-carousel" class="carousel slide" data-ride="carousel">
	<ol class="carousel-indicators">
		<li data-target="#latest-news-carousel" data-slide-to="0" class="active"></li>
		<?php for($i=1; $i<count($list); $i++) : ?>
			<li data-target="#latest-news-carousel" data-slide-to="<?php echo $i; ?>"></li>
		<?php endfor; ?>
	</ol>







	<div class="carousel-inner" role="listbox">
		<?php
		$fist = true;
		foreach ($list as $item) : 
			$images = json_decode($item->images);
		$images_url = $images->image_intro;
		$images_alt = $images->image_intro_alt;
		$intro = explode(" ", strip_tags($item->fulltext));
		$date = new DateTime($item->created);
		?>
		<div class="item <?php if($fist) {echo ' active'; $fist=false;}?>">
			<img src="<?php echo $images_url; ?>" alt="<?php echo $images_alt; ?>" class="img-responsive">
			<div class="carousel-caption">
				<div class="title"><?php echo $item->title; ?></div>
				<div class="date"><?php echo $date->format('F d, Y'); ?></div>
				<div class="intro">
					<?php 
					for ($i = 0; ($i < 15 && $i < count($intro)); $i++) {
						echo $intro[$i] . " ";
					}
					?>...
				</div>
				<div class="read-more">
					<a href="<?php echo $item->link; ?>" >Skaityti daugiau</a>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>


<a class="left carousel-control" href="#latest-news-carousel" role="button" data-slide="prev">
	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	<span class="sr-only">Previous</span>
</a>
<a class="right carousel-control" href="#latest-news-carousel" role="button" data-slide="next">
	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	<span class="sr-only">Next</span>
</a>
</div>