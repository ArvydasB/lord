<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<div class="container">
    <div id="latestBlogArticles">
        <h4>Recommended reading</h4>
        <?php foreach ($list as $item) : ?>
        
            <?php
                $image = json_decode($item->images);
                $date = date_create($item->displayDate);
            ?>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding latest-blog-post">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 post-img">
                     <a href="<?php echo $item->link; ?>"><img src="/<?php echo $image->{"image_intro"}; ?>" alt="<?php echo $image->{"image_intro_alt"}; ?>"></a>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 blog-content">
                    <div class="post-title">
                        <a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
                    </div>
                    <div class="post-date">
                        <?php echo date_format($date, 'm/d/Y'); ?>
                    </div>
                    <div class="post-intro">
                        <?php echo $item->displayIntrotext; ?>
                    </div>
                    <div class="post-readmore">
                        <a href="<?php echo $item->link; ?>">
        <span class="header-btn-text-read">Read more</span><span class="sprite sprite-link-arrow inlinedisplay"></span></a>
                    </div>
                </div>
            </div>              
            
        <?php endforeach; ?>
    </div>
</div>