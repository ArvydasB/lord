<?php defined('_JEXEC') or die; 
	error_reporting(E_ALL);
ini_set('display_errors', 1);
?>

<div class="landing-page-intros">
	<?php foreach ($list as $item) : ?>
		<?php
			if($item->active == "active") continue; 
			preg_match_all("/<p>.*?<\/p>/s", $item->introtext, $matches);
			$intro = implode('', $matches[0]);
		?>
		<div class="landing-page-intro">
			<h2><?php echo $item->title; ?></h2>
			<?php echo $intro; ?>
			<div class="readmore-dark">
				<a href="<?php echo $item->link; ?>">Read More</a>
			</div>
		</div>
	<?php endforeach; ?>
</div>